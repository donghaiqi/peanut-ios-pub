//
//  HTTPrequest.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/4/26.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ZTHttp  [HTTPrequest sharedInstance]

@interface HTTPrequest : NSObject
AS_SINGLETON(HTTPrequest);

#pragma mark - 工具
//城市接口
-(void)getCitylist_success:(void (^_Nonnull)(NSDictionary *dic))success
           failure_Nonnull:(void (^_Nonnull)(NSDictionary * _Nonnull dic))failure;


#pragma mark - 登录
//获取验证码
-(void)getCodeByPhoneNum:(NSString *_Nullable)phoneNum
                 success:(void (^_Nonnull)(NSDictionary * _Nullable dic))success
         failure_Nonnull:(void (^_Nonnull)(NSDictionary * _Nonnull dic))failure;

//登录获取token
-(void)getMobileToken:(NSString *_Nullable)phoneNum
              smsCode:(NSString *_Nullable)smsCode
              success:(void (^_Nonnull)(NSDictionary * _Nullable dic))success
      failure_Nonnull:(void (^_Nonnull)(NSDictionary * _Nonnull dic))failure;


#pragma mark - 首页

//获取首页滚动广告
//place：广告位置0-下拉选项,1-APP首页轮播,2-启动页,3-弹窗
-(void)getAdvImage:(int)place
           success:(void (^_Nonnull)(NSDictionary * _Nullable dic))success
   failure_Nonnull:(void (^_Nonnull)(NSDictionary * _Nonnull dic))failure;

//用户点击App广告
-(void)clickAdvImage:(int)imgId
             success:(void (^_Nonnull)(NSDictionary * _Nullable dic))success
     failure_Nonnull:(void (^_Nonnull)(NSDictionary * _Nonnull dic))failure;


#pragma mark - 商品搜索
//商品搜索接口
-(void)search_Goods:(NSString *_Nullable)keyword//商品关键字
             orderBy:(NSString *_Nullable)orderBy//排序综合:common,销量:sale,价格price,默认common
           ascOrDesc:(NSString *_Nullable)ascOrDesc//排序类型升序:asc降序:desc
             pageNum:(int)pageNum
            pageSize:(int)pageSize
        success:(void (^_Nonnull)(NSDictionary * _Nullable dic))success
failure_Nonnull:(void (^_Nonnull)(NSDictionary * _Nonnull dic))failure;

//商品搜索记录
-(void)http_mySearchHistory_success:(void (^_Nonnull)(NSDictionary * _Nullable dic))success
failure_Nonnull:(void (^_Nonnull)(NSDictionary * _Nonnull dic))failure;

//清空搜索记录
-(void)http_clearSearchHistory_success:(void (^_Nonnull)(NSDictionary * _Nullable dic))success
failure_Nonnull:(void (^_Nonnull)(NSDictionary * _Nonnull dic))failure;



#pragma mark - 商品相关
//商品分类信息
-(void)get_Goods_Type_map:(int)goodsType //1-服务销售类型,2-商品销售类型
                    level:(int)level    //2-3
                    parentId:(int)parentId
        success:(void (^_Nonnull)(NSDictionary * _Nullable dic))success
failure_Nonnull:(void (^_Nonnull)(NSDictionary * _Nonnull dic))failure;



//商品详情
-(void)get_Goods_Detail_BygoodsNo:(NSString * _Nonnull )goodsNo
                          success:(void (^_Nonnull)(NSDictionary * _Nullable dic))success
                  failure_Nonnull:(void (^_Nonnull)(NSDictionary * _Nonnull dic))failure;




#pragma mark - 回收
//查询回收分类列表
-(void)recycle_Gategory_List_success:(void (^_Nonnull)(NSDictionary * _Nullable dic))success
                     failure_Nonnull:(void (^_Nonnull)(NSDictionary * _Nonnull dic))failure;

//查询回收品牌列表
-(void)recycle_brand_list:(int)cateId//分类
                  success:(void (^_Nonnull)(NSDictionary * _Nullable dic))success
          failure_Nonnull:(void (^_Nonnull)(NSDictionary * _Nonnull dic))failure;

//回收型号列表
-(void)recycle_model_list:(int)cateId
                  brandId:(int)brandId//品牌
                  success:(void (^_Nonnull)(NSDictionary * _Nullable dic))success
          failure_Nonnull:(void (^_Nonnull)(NSDictionary * _Nonnull dic))failure;
@end

