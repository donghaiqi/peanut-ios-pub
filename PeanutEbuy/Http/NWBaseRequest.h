/**
 * @brief   Base URL request to Encode and decode
 * @author  Zq
 * @date
 */

#import <EasyIOS/Request.h>

@interface NWBaseRequest : Request

@property (nonatomic, assign) BOOL needPage;
@property (nonatomic, assign) BOOL isThirdHttp;


- (void)startRequest;

// 去除基本信息, 比如错误码和错误信息后获得的接口信息

- (id)getIsSuccess;

//设置请求头
- (void)headerRequest;

- (id)getContent;

- (NSString *)errorString;

// 从本地缓存中加载上次的数据到output属性中
- (void)loadCache;

- (void)clearCache;

- (void)switchServer:(BOOL)releaseServer;

@end
