#import "NWBaseRequest.h"
#import "XAspect.h"
#import "AppDelegate.h"
#import "TMCache.h"





//m.mall.flosars.com//积分商城

#define RIGHT_CODE 200
#define CODE_KEY @"resultcode"
#define CONTENT_KEY @"result"
#define MESSAGE_KEY @"errordes"

@implementation NWBaseRequest


- (void)loadRequest {
    [super loadRequest];

    // 添加这句代码
    self.HOST = HTTP_API;
//    self.isJSONRequestSerializer = NO;

    self.METHOD = @"Post";
    self.needCheckCode = YES;

    self.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/plain", @"text/javascript", @"text/json",@"text/html", nil];
}

- (void)headerRequest
{
//    UserInfo * test  = [UserInfo MR_findFirst];
    NSMutableDictionary * httpHeaderdic = [[NSMutableDictionary alloc]init];
    
//    UserInfo* userInfo   = [ZTCore getUserInfo];
//    if (userInfo) {
//        [httpHeaderdic setObject:userInfo.token forKey:@"token"];
//        [httpHeaderdic setObject:userInfo.uid forKey:@"uid"];
//    }
    self.httpHeaderFields  = [NSDictionary dictionaryWithDictionary:httpHeaderdic];
}

- (void)loadCache {
    self.output = [[TMCache sharedCache] objectForKey:[self cacheKey]];
}

- (void)clearCache {
    [[TMCache sharedCache] removeObjectForKey:[self cacheKey]];
    self.output = nil;
}

-(NSMutableDictionary *)requestParams{
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.params];
    
    return dict;
}


- (void)startRequest{
//    if ([ZTTools checkNetworkState]) {
        self.requestNeedActive = YES;
//    }
}

- (id)getIsSuccess{
    if (self.output) {
        if (_isThirdHttp) {
            return self.output[@"code"];
        }else{
            return self.output[CODE_KEY];
        }
     
    }
    return nil;
}


- (id)getContent {
    if (self.output) {
//        ZTLog(@"返回结果：%@",self.output);
        if (_isThirdHttp) {
            return self.output[@"data"];
        }else{
            return self.output[CONTENT_KEY];
        }
    }
    return nil;
}

- (NSString *)errorString {
    ZTLog(@"返回结果：%@",self.output);

    if (self.error) {
        return self.error.localizedDescription;
    }else if ([self.message isNotEmpty]){
        return self.message;
    }else{
        return @"请求发生错误";
    }
}

- (NSString *)cacheKey {
    NSString *className = NSStringFromClass([self class]);
    return [NSString stringWithFormat:@"HTTP_REQUEST_%@", className];
}

-(void)switchServer:(BOOL)releaseServer {
    NSString * str;
    if (_isThirdHttp) {
        str = @"code";
    }else{
        str = CODE_KEY;
    }
    
    /*
    if (releaseServer) {
        [Action actionConfigHost:HTTP_API_TEST__RELEASE client:@"Bocai" codeKey:str rightCode:RIGHT_CODE msgKey:MESSAGE_KEY];
    } else {
        [Action actionConfigHost:self.HOST client:@"控客测试" codeKey:str rightCode:RIGHT_CODE msgKey:MESSAGE_KEY];
    }*/
    
    [Action actionConfigHost:self.HOST client:@"控客测试" codeKey:str rightCode:RIGHT_CODE msgKey:MESSAGE_KEY];
}

@end

#define AtAspect HTTPRequest
#define AtAspectOfClass AppDelegate
@classPatchField(AppDelegate)

AspectPatch(-, void, application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions) {
    NWBaseRequest * temp = [[NWBaseRequest alloc]init];
    [temp switchServer:NO];
    
//    [NWBaseRequest switchServer:NO];
    XAMessageForward(application:application didFinishLaunchingWithOptions:launchOptions);
}
@end

#undef AtAspectOfClass
#undef AtAspect
