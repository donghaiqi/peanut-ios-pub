//
//  OrderEidtVC.m
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/4/9.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import "OrderEidtVC.h"
#import "TYTabPagerView.h"
#import "StoreCell.h"
#import "MailInfoCell.h"
#import "MailAdressCell.h"
#import "MailProductCell.h"
#import "StoreGetVeiw.h"


@interface OrderEidtVC ()<TYTabPagerViewDataSource, TYTabPagerViewDelegate,UITableViewDelegate,UITableViewDataSource>{
       NSArray * testArray;
}
@property (nonatomic, strong) NSArray *datas;
@property (nonatomic, strong) TYTabPagerView *pagerView;
@property(nonatomic,strong)UITableView * tableView;

@property(nonatomic,strong)UIView * goStore_BackView;
@property(nonatomic,strong)StoreGetVeiw * goStoreGetVeiw;



@end

@implementation OrderEidtVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"订单填写";
    self.view.backgroundColor = [UIColor mainBackgroundColor];

    [self addTabPagerView];
    [self loadData];
    
    [self addbuttomView];
    
    //商家邮寄
    testArray = @[@"1",@"2",@"3",];
    [self creatTableview];
    
    //到店自取
    [self creat_StoreGetVeiw];
}

- (void)addTabPagerView {

    _pagerView = [[TYTabPagerView alloc]init];
    _pagerView.backgroundColor = [UIColor whiteColor];
    _pagerView.tabBarHeight = 42;
    _pagerView.tabBar.layout.barStyle = TYPagerBarStyleProgressBounceView;
    _pagerView.tabBar.layout.cellWidth = CGRectGetWidth(self.view.frame)/2;
    _pagerView.tabBar.layout.cellSpacing = 0;
    _pagerView.tabBar.layout.cellEdging = 0;
    _pagerView.tabBar.layout.selectedTextColor = [UIColor blackColor];
    _pagerView.tabBar.layout.progressColor = [UIColor mainBlueColor];
    _pagerView.tabBar.layout.adjustContentCellsCenter = YES;
    _pagerView.dataSource = self;
    _pagerView.delegate = self;
    [self.view addSubview:_pagerView];
//    _pagerView = pagerView;
    _pagerView.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame),CGRectGetHeight(self.view.frame)-CGRectGetMaxY(self.navigationController.navigationBar.frame)-60);
}

- (void)loadData {
    NSArray * datas = @[@"商家邮寄",@"到店自取"];
    _datas = [datas copy];
//    [self scrollToControllerAtIndex:0 animate:YES];
    [_pagerView reloadData];
}

-(void)addbuttomView{
    UIView * bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetHeight(_pagerView.frame), self.view.width, 60)];
    bottomView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bottomView];
    
    UILabel * priceLabel = [[UILabel alloc]init];
    priceLabel.textColor = RGBA(254, 88, 80, 1);
    priceLabel.text = @"￥10293.00";
    priceLabel.font = [UIFont systemFontOfSize:19];
    [bottomView addSubview:priceLabel];
    
    UIButton * orderBtn = [[UIButton alloc]init];
    [orderBtn setTitle:@"提交订单" forState:UIControlStateNormal];
    [orderBtn.layer setMasksToBounds:YES];
    [orderBtn.layer setCornerRadius:4];
    [orderBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [orderBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    orderBtn.backgroundColor = [UIColor mainBlueColor];
    [bottomView addSubview:orderBtn];
    
    [orderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.offset(100);
        make.height.offset(32);
        make.centerY.equalTo(bottomView);
        make.right.equalTo(bottomView.mas_right).offset(-20);
    }];
    
    [priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(17);
        make.top.offset(20);
        make.height.offset(27);
        make.centerY.equalTo(bottomView);
    }];
}

-(void)creatTableview{
    UITableViewStyle style;
      if (@available(iOS 13.0, *)) {
          style =UITableViewStyleInsetGrouped;
      } else {
          style = UITableViewStyleGrouped;
      }
    self.tableView = [[UITableView alloc]initWithFrame:CGRectZero style:style];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate =self;
    self.tableView.dataSource =self;
    self.tableView.backgroundColor = [UIColor mainBackgroundColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    [self.tableView registerNib:[UINib nibWithNibName:@"MailAdressCell" bundle:nil] forCellReuseIdentifier:NSStringFromClass([MailAdressCell class])];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"StoreCell" bundle:nil] forCellReuseIdentifier:NSStringFromClass([StoreCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:@"MailProductCell" bundle:nil] forCellReuseIdentifier:NSStringFromClass([MailProductCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:@"MailInfoCell" bundle:nil] forCellReuseIdentifier:NSStringFromClass([MailInfoCell class])];

    self.tableView.sectionHeaderHeight = 0;
    self.tableView.sectionFooterHeight = 15;
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 10)];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];

    _tableView.backgroundColor =[UIColor mainBackgroundColor];
}

-(void)creat_StoreGetVeiw{
    _goStore_BackView = [[UIView alloc]init];
    _goStore_BackView.backgroundColor = [UIColor mainBackgroundColor];
    _goStoreGetVeiw = [StoreGetVeiw instanceView];
    _goStoreGetVeiw.backgroundColor = [UIColor clearColor];
    [_goStore_BackView addSubview:_goStoreGetVeiw];
    [_goStoreGetVeiw mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(10);
        make.left.offset(10);
        make.right.offset(-10);
        make.height.offset(210);
    }];
    
}


#pragma mark- TYTabPagerViewDelegate
- (NSInteger)numberOfViewsInTabPagerView {
    return _datas.count;
}

- (UIView *)tabPagerView:(TYTabPagerView *)tabPagerView viewForIndex:(NSInteger)index prefetching:(BOOL)prefetching {
    CGRect frame = [tabPagerView.layout frameForItemAtIndex:index];

    if (index == 0) {
        self.tableView.frame =frame;
        return self.tableView;
    }else{
        _goStore_BackView.frame =frame;
        return _goStore_BackView;
    }
}

- (NSString *)tabPagerView:(TYTabPagerView *)tabPagerView titleForIndex:(NSInteger)index {
    NSString *title = _datas[index];
    return title;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }else{
        return testArray.count;
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 93;
    }else{
        if (indexPath.row ==0) {
            return 35;
        }else if (indexPath.row == testArray.count-1){
            return 110;
        }else{
            return 95;
        }
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        MailAdressCell *addressCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MailAdressCell class])];
        if(!addressCell){
            addressCell = [[NSBundle mainBundle]loadNibNamed:@"MailAdressCell" owner:nil options:nil].firstObject;
        }
        return addressCell;

    }else{
        if (indexPath.row ==0) {
            StoreCell *storeCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([StoreCell class])];
            if(!storeCell){
                storeCell = [[NSBundle mainBundle]loadNibNamed:@"StoreCell" owner:nil options:nil].firstObject;
            }
            [storeCell setListStyle:@"List_By_Order"];
            storeCell.shopNameLabel.text =@"花生易购旗舰店";
            return storeCell;
        }else if (indexPath.row == testArray.count-1){
            MailInfoCell *mailInfoCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MailInfoCell class])];
            if(!mailInfoCell){
                mailInfoCell = [[NSBundle mainBundle]loadNibNamed:@"MailInfoCell" owner:nil options:nil].firstObject;
            }
            return mailInfoCell;
        }else{
            MailProductCell *mailProductCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MailProductCell class])];
            if(!mailProductCell){
                mailProductCell = [[NSBundle mainBundle]loadNibNamed:@"MailProductCell" owner:nil options:nil].firstObject;
            }
            return mailProductCell;
        }
    }
}

@end
