//
//  MailInfoCell.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/4/10.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MailInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *orderMsg;
@property (weak, nonatomic) IBOutlet UILabel *mailPrice;
@property (weak, nonatomic) IBOutlet UILabel *orderPrice;

@end

NS_ASSUME_NONNULL_END
