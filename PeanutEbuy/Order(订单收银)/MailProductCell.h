//
//  MailProductCell.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/4/10.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MailProductCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *cell_imgV;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (weak, nonatomic) IBOutlet UIView *productNumView;

@end

NS_ASSUME_NONNULL_END
