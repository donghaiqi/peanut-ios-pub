//
//  StoreGetVeiw.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/4/11.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface StoreGetVeiw : UIView
@property (weak, nonatomic) IBOutlet UIView *bodyBackView;
+(StoreGetVeiw*)instanceView;
@end

NS_ASSUME_NONNULL_END
