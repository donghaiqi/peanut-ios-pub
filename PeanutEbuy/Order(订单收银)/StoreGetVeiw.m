//
//  StoreGetVeiw.m
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/4/11.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import "StoreGetVeiw.h"

@implementation StoreGetVeiw
+(StoreGetVeiw*)instanceView{
    StoreGetVeiw* view= [[[NSBundle mainBundle] loadNibNamed:@"StoreGetVeiw" owner:nil options:nil] objectAtIndex:0];
      view.bodyBackView.backgroundColor = [UIColor whiteColor];
      [view.bodyBackView.layer setCornerRadius:10];
      [view.bodyBackView.layer setMasksToBounds:YES];
    return view;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
