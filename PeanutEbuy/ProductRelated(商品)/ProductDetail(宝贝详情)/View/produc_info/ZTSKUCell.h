//
//  ZTSKUCell.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/5/12.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ShowMoreBlcok)(void);

NS_ASSUME_NONNULL_BEGIN

@interface ZTSKUCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIStackView *stackView;
@property (weak, nonatomic) IBOutlet UIImageView *imgV1;
@property (weak, nonatomic) IBOutlet UIImageView *imgV2;
@property (weak, nonatomic) IBOutlet UIImageView *imgV3;
@property (weak, nonatomic) IBOutlet UIImageView *imgV4;
@property (weak, nonatomic) IBOutlet UIImageView *imgV5;

@property (weak, nonatomic) IBOutlet UILabel *skuLabel;

@property (weak, nonatomic) IBOutlet UIButton *showMoreBtn;
- (IBAction)clickShowMore:(id)sender;


@property(nonatomic,strong)NSArray * skuArray;
@property(nonatomic,copy)ShowMoreBlcok  showMoreBlcok;


@end

NS_ASSUME_NONNULL_END
