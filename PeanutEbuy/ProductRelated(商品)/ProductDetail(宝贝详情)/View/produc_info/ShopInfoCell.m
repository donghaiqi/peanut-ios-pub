//
//  ShopInfoCell.m
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/5/12.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import "ShopInfoCell.h"

@implementation ShopInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setShopInfoModel:(ShopExtInfoModel *)tempModel{
    if (tempModel) {
        [_shopImgV sd_setImageWithURL:[NSURL URLWithString:tempModel.logoUrl]];
        _shopTitleLabel.text = tempModel.showName;
        _shopInfoLabel.text = tempModel.showTitle;
        _shopInfoModel = tempModel;
    }
}


- (IBAction)clickGetINBtn:(id)sender {
}
@end
