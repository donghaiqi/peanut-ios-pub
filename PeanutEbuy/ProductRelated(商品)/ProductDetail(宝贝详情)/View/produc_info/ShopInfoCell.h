//
//  ShopInfoCell.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/5/12.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductModel.h"

NS_ASSUME_NONNULL_BEGIN
@interface ShopInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *shopImgV;
@property (weak, nonatomic) IBOutlet UILabel *shopTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *shopInfoLabel;
@property (weak, nonatomic) IBOutlet UIButton *getInBtn;
- (IBAction)clickGetINBtn:(id)sender;

@property(nonatomic,strong)ShopExtInfoModel * shopInfoModel;

@end

NS_ASSUME_NONNULL_END
