//
//  ZTSKUCell.m
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/5/12.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import "ZTSKUCell.h"
#import "ProductModel.h"

@class SKUInfoModel;
@implementation ZTSKUCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setSkuArray:(NSArray *)skuArray{
    if (skuArray) {
        
        for (int a= 0; a<skuArray.count; a++) {
            for (UIImageView *imageV in _stackView.subviews) {
                if (imageV.tag == a) {
                    SKUInfoModel * model = [skuArray objectAtIndex:a];
                    [imageV sd_setImageWithURL:[NSURL URLWithString:model.imgUrl]];
                }
            }
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickShowMore:(id)sender {
    if(_showMoreBlcok){
        _showMoreBlcok();
    }
        
}
@end
