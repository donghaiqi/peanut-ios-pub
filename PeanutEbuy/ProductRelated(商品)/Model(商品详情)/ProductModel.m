//
//  ProductModel.m
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/5/10.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import "ProductModel.h"

@implementation ProductModel

+ (NSDictionary *)mj_objectClassInArray{
    return @{@"goodSKUAttrList": @"GoodSKUModel",
             @"goodsSKUInfoList": @"SKUInfoModel",
             @"subImgIds": @"SubImgModel"
               };
}

@end


@implementation GoodSKUModel

@end

@implementation SKUInfoModel

//+ (NSDictionary *)mj_objectClassInArray{
//    return @{@"goodSKUAttr1": [GoodSKUModel class],
//             @"goodSKUAttr2": [GoodSKUModel class],
//               };
//}
@end

@implementation ShopExtInfoModel

@end

@implementation SubImgModel

@end
