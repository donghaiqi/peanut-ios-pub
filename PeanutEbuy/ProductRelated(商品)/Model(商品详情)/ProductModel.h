//
//  ProductModel.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/5/10.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class GoodSKUModel;
@class SKUInfoModel;
@class ShopInfoModel;
@class ShopExtInfoModel;
@class SubImgModel;

@interface ProductModel : NSObject
 

@property(nonatomic,copy)NSString * auditResult;  //审核不通过结果
@property(nonatomic,assign)int auditStatus;  //0审核通过，1审核不通过
@property(nonatomic,assign)int brandId;  //品牌id，brand表主键
@property(nonatomic,copy)NSString * brandName;  //品牌名称
@property(nonatomic,copy)NSString * createTime;  //创建时间
@property(nonatomic,copy)NSString * detailContent;  //商品详情富文本
@property(nonatomic,assign)int freightFeeTemplateId;  //运费模板
@property (nonatomic , strong) NSArray<GoodSKUModel *> * goodSKUAttrList;//该商品的所有规格

@property(nonatomic,copy)NSString * goodsName;  //商品名称
@property(nonatomic,copy)NSString * goodsNo;  //商品货号标号
@property(nonatomic,assign)int goodsPrice;  //运费模板
@property (nonatomic , strong) NSArray<SKUInfoModel *> * goodsSKUInfoList;//商品sku信息

@property(nonatomic,assign)int goodsType;  //商品类型，1.服务销售类2.商品销售类
@property(nonatomic,assign)int homeDelivery;  //是否送货到家，默认0，0-否1-是
@property(nonatomic,assign)int homeDeliveryRange;  //送到到家的最大公里数

@property(nonatomic,assign)int id;  //商品id
@property(nonatomic,copy)NSString * keyWords;  //商品关键字
@property(nonatomic,assign)int mainImgId;  //商品主图id
@property(nonatomic,copy)NSString * mainImgUrl;  //商品主图url

@property(nonatomic,assign)int oneCategoryId;  //一级类目
@property(nonatomic,copy)NSString * oneCategoryName;  //一级类目名称
@property(nonatomic,assign)int twoCategoryId;  //二级类目
@property(nonatomic,copy)NSString * twoCategoryName;  //二级类目名称
@property(nonatomic,assign)int weight;  //重量


@property(nonatomic,assign)int secondHand;  //是否二手货
@property(nonatomic,assign)int sellNum;  //是否二手货

@property(nonatomic,assign)int shopId;  //店铺ID
@property(nonatomic,copy)NSString * shopName;  //店铺名称
@property(nonatomic,strong)ShopInfoModel * shopInfoVO;  //店铺model
@property(nonatomic,strong)ShopExtInfoModel * shopExtInfoVO; //店铺扩展信息


@property(nonatomic,assign)int sort;  //商品排序位置
@property(nonatomic,assign)int status;  //商品状态.0-上架1-下架
@property(nonatomic,assign)int stock;  //商品总库存

@property (nonatomic , strong) NSArray<SubImgModel *> * subImgIds;//商品图片集合

@property(nonatomic,copy)NSString * subName;  //店铺名称

@end

NS_ASSUME_NONNULL_END

NS_ASSUME_NONNULL_BEGIN
@interface GoodSKUModel : NSObject
@property(nonatomic,copy)NSString * goodAttrKey;  //商品SKU_KEY
@property(nonatomic,assign)int goodAttrKeyId;  //商品排序位置
@property(nonatomic,copy)NSString * goodsAttrValue;  //商品SKU_value
@end
NS_ASSUME_NONNULL_END


NS_ASSUME_NONNULL_BEGIN
@interface SKUInfoModel : NSObject
//@property (nonatomic , strong) NSArray<GoodSKUModel *> * goodSKUAttr1;//规格数组1
//@property (nonatomic , strong) NSArray<GoodSKUModel *> * goodSKUAttr2;//规格数组2
@property (nonatomic , strong) GoodSKUModel * goodSKUAttr1;//规格数组1
@property (nonatomic , strong) GoodSKUModel * goodSKUAttr2;//规格数组2
@property(nonatomic,assign)int id;  //sku_id
@property(nonatomic,assign)int imgId;  //类型图片Id
@property(nonatomic,copy)NSString * imgUrl;  //图片IMG_Url
@property(nonatomic,assign)int marketPrice;  //市场类型价格
@property(nonatomic,copy)NSString * skuNo;  //sku编号
@property(nonatomic,assign)int stock;  //库存
@property(nonatomic,assign)int stockWarn;  //危险库存
@property(nonatomic,assign)int wholesalePrice;  //批发价格
@end
NS_ASSUME_NONNULL_END

NS_ASSUME_NONNULL_BEGIN
@interface ShopExtInfoModel : NSObject
@property(nonatomic,copy)NSString * showName;  //展示的商家名称
@property(nonatomic,copy)NSString * showTitle;  //商家标题
@property(nonatomic,copy)NSString * shopDesc;  //商家描述
@property(nonatomic,copy)NSString * keyWords;  //关键字
@property(nonatomic,assign)int logoId;  //
@property(nonatomic,copy)NSString * logoUrl;  //
@property(nonatomic,copy)NSString * introduce;  //店铺介绍

@end
NS_ASSUME_NONNULL_END

NS_ASSUME_NONNULL_BEGIN
@interface SubImgModel : NSObject
@property(nonatomic,assign)int key;  //图片ID
@property(nonatomic,copy)NSString * value;  //图片路径

@end
NS_ASSUME_NONNULL_END


