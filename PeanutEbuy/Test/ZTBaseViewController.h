//
//  EDUBaseViewController.h
//  Students
//
//  Created by xuchangyuan on 15/5/16.
//  Copyright (c) 2015年 xuchangyuan. All rights reserved.

#import <UIKit/UIKit.h>
#import "ZTNavView.h"

@interface ZTBaseViewController : UIViewController

@property(nonatomic,retain)ZTNavView *navView;
@property(nonatomic,retain)UIView *contentView;

@property (nonatomic, strong)UIButton *leftButton;
@property (nonatomic, retain)UIButton *rightButton;
@property(nonatomic,retain)UIPanGestureRecognizer *panGesture;
@property (nonatomic, assign)BOOL isLoad;

- (UIButton *)leftBarButtonWithTitle:(NSString *)title
                         barNorImage:(UIImage *)norImage
                         barHltImage:(UIImage *)hltImage
                              action:(void(^)(void))actionBlock;

- (UIButton *)rightBarButtonWithTitle:(NSString *)title
                          barNorImage:(UIImage *)norImage
                          barHltImage:(UIImage *)hltImage
                               action:(void(^)(void))actionBlock;

@end
