


#import "TestViewController.h"
#import <XAspect/XAspect.h>


//测试页面
#import "LoginViewController.h"
#import "ZTtabbarCtl.h"
#import "ChooseTestViewController.h"
#import "HomeVC.h"
#import "CXSearchViewController.h"
#import "CityList.h"
#import "ProductListViewController.h"
#import "ShoppingCarViewController.h"
#import "ClassificationViewController.h"
#import "OCRViewController.h"
#import "AddressVC.h"
#import "CashVC.h"
#import "OrderEidtVC.h"
#import "SearchResultVC.h"
#import "RecyclingVC.h"
#import "RecyclingSecondVC.h"
#import "PersonalCenterVC.h"
#import "ValuationVC.h"

#import "CityModel.h"








@interface TestViewController (){
    
    
}

@property (strong, nonatomic) RETableViewManager *manager;


@end

@implementation TestViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"测试页面";
    self.navigationController.navigationBar.hidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    
//  __typeof (&*self) __weak weakSelf = self;

    self.manager = [[RETableViewManager alloc] initWithTableView:self.tableView];
    self.manager.delegate = self;
    
    RETableViewSection *section = [RETableViewSection section];
    [self.manager addSection:section];
    [section addItem:[RETableViewItem itemWithTitle:@"主流程" accessoryType:UITableViewCellAccessoryDisclosureIndicator selectionHandler:^(RETableViewItem *item) {
        LoginViewController * CTL = (LoginViewController *) [SceneMap LoginScene];
        [self.navigationController pushViewController:CTL animated:YES];
    }]];
    [section addItem:[RETableViewItem itemWithTitle:@"网络代码封装测试" accessoryType:UITableViewCellAccessoryDisclosureIndicator selectionHandler:^(RETableViewItem *item) {
        [ZTHttp getCodeByPhoneNum:@"15068142677" success:^(NSDictionary *dic) {

        } failure_Nonnull:^(NSDictionary *dic) {
            ZTLog(@"测试");
        }];
    }]];
    
    
    [section addItem:[RETableViewItem itemWithTitle:@"城市列表自封装Plist" accessoryType:UITableViewCellAccessoryDisclosureIndicator selectionHandler:^(RETableViewItem *item) {
        [ZTHttp getCitylist_success:^(NSDictionary *dic) {
        if (dic) {
            NSMutableArray * cityArray = [CityModel mj_objectArrayWithKeyValuesArray:[dic objectForKey:@"data"]];
            //筛选2级城市
            NSMutableArray * cityLevel2 =[[NSMutableArray alloc]init];//2级别城市
            for (int a= 0; a<cityArray.count; a++) {
                CityModel * model = [cityArray objectAtIndex:a];
                if(model.level == 2){
                    [cityLevel2 addObject:model];
                }
            }
            //筛选相同首字母归位一个数组
            NSMutableArray *dateMutablearray = [[NSMutableArray alloc]init];
            for (int a=0; a<cityLevel2.count; a++) {
                CityModel * model = [cityLevel2 objectAtIndex:a];
                NewCityModel * newModel = [[NewCityModel alloc]init];
                newModel.cities = [[NSMutableArray alloc]init];
//                    NSMutableArray * newcityArray = [[NSMutableArray alloc]init];
                [newModel.cities addObject:model];
                newModel.title = model.letter;
                
                for (int s=a+1; s<cityLevel2.count; s++) {
                    CityModel * smodel = [cityLevel2 objectAtIndex:s];
                    if ([smodel.letter isEqualToString:model.letter]==YES) {
                        [newModel.cities addObject:smodel];
                        [cityLevel2 removeObjectAtIndex:s];
                        s-=1;
                    }
                }
                [dateMutablearray addObject:newModel];
            }
            
            //筛选后的数组按照A-Z排序
            NSMutableArray * array =[NSMutableArray arrayWithArray:[dateMutablearray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                NewCityModel * newModel1 = (NewCityModel *)obj1;
                NewCityModel * newModel2 = (NewCityModel *)obj2;

                return [newModel1.title compare:newModel2.title options:NSNumericSearch];
            }]];
            
            //遍历后的结果存入plist文件
            NSMutableArray * data =[NewCityModel mj_keyValuesArrayWithObjectArray:array];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *path = [documentsDirectory stringByAppendingPathComponent:@"PeanutEbuyCity.plist"];
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
//            NSMutableDictionary *data;
            if (![fileManager fileExistsAtPath: path]) {
//                [data writeToFile: path atomically:YES];//写入Plist文件成功
            }

            
            
            
            }
        } failure_Nonnull:^(NSDictionary * _Nonnull dic) {
            
        }];
        
    }]];

    
    [section addItem:[RETableViewItem itemWithTitle:@"网络测试用例" accessoryType:UITableViewCellAccessoryDisclosureIndicator selectionHandler:^(RETableViewItem *item) {
        
        NSString *string  = [NSString stringWithFormat:@"%@",@"http://service.umonbe.cn/login/sms/verification-code"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.securityPolicy.allowInvalidCertificates = YES;
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer    = [AFJSONResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", nil];
        // 设置超时时间
        manager.requestSerializer.timeoutInterval = 30.0f;
        
        NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
        [dic setObject:@"13758101494" forKey:@"mobile"];
//        NSString * jsonStr =[self convertToJsonData:dic];
        [manager POST:string parameters:dic success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            int code = [[responseObject objectForKey:@"code"]intValue];
            if (code == 200) {
                ZTLog(@"请求结果：%@ .",[responseObject objectForKey:@"message"]);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            ZTLog(@"失败");
        }];

//
    }]];

    
    

    
    [section addItem:[RETableViewItem itemWithTitle:@"登陆页1" accessoryType:UITableViewCellAccessoryDisclosureIndicator selectionHandler:^(RETableViewItem *item) {
        LoginViewController * CTL = (LoginViewController *) [SceneMap LoginScene];
        [self.navigationController pushViewController:CTL animated:YES];
    }]];
    
    
    [section addItem:[RETableViewItem itemWithTitle:@"菜单" accessoryType:UITableViewCellAccessoryDisclosureIndicator selectionHandler:^(RETableViewItem *item) {
        ZTtabbarCtl * CTL = (ZTtabbarCtl *) [SceneMap ZTtabbarCtlScene];
//        [self.navigationController pushViewController:CTL animated:YES];
//        ZTtabbarCtl * CTL = (ZTtabbarCtl *) [SceneMap ZTtabbarCtlScene];
        [URLNavigation setRootViewController:CTL];
    }]];
    
    [section addItem:[RETableViewItem itemWithTitle:@"首页1-标签搜索页" accessoryType:UITableViewCellAccessoryDisclosureIndicator selectionHandler:^(RETableViewItem *item) {
        CXSearchViewController *CTL = [[CXSearchViewController alloc] init];
        [self.navigationController pushViewController:CTL animated:YES];
    }]];
    
    
    
    [section addItem:[RETableViewItem itemWithTitle:@"商品配置页" accessoryType:UITableViewCellAccessoryDisclosureIndicator selectionHandler:^(RETableViewItem *item) {
          ChooseTestViewController * CTL =  [[ChooseTestViewController alloc]init];
          CTL.view.backgroundColor = [UIColor whiteColor];
        [self.navigationController pushViewController:CTL animated:YES];
    }]];
    
    [section addItem:[RETableViewItem itemWithTitle:@"商品详情页" accessoryType:UITableViewCellAccessoryDisclosureIndicator selectionHandler:^(RETableViewItem *item) {
        ProductListViewController * CTL =  [[ProductListViewController alloc]init];
        [self.navigationController pushViewController:CTL animated:YES];
    }]];
    
    [section addItem:[RETableViewItem itemWithTitle:@"购物车" accessoryType:UITableViewCellAccessoryDisclosureIndicator selectionHandler:^(RETableViewItem *item) {
        ShoppingCarViewController * CTL =  [[ShoppingCarViewController alloc]init];
        [self.navigationController pushViewController:CTL animated:YES];
    }]];
    
    [section addItem:[RETableViewItem itemWithTitle:@"商品详情页" accessoryType:UITableViewCellAccessoryDisclosureIndicator selectionHandler:^(RETableViewItem *item) {
        ProductListViewController * CTL =  [[ProductListViewController alloc]init];
        [self.navigationController pushViewController:CTL animated:YES];
    }]];
    
    [section addItem:[RETableViewItem itemWithTitle:@"二级目录分类" accessoryType:UITableViewCellAccessoryDisclosureIndicator selectionHandler:^(RETableViewItem *item) {
         ClassificationViewController * CTL =  [[ClassificationViewController alloc]init];
         [self.navigationController pushViewController:CTL animated:YES];
     }]];
    
    [section addItem:[RETableViewItem itemWithTitle:@"OCR信息验证" accessoryType:UITableViewCellAccessoryDisclosureIndicator selectionHandler:^(RETableViewItem *item) {
         OCRViewController * CTL =  [[OCRViewController alloc]init];
         [self.navigationController pushViewController:CTL animated:YES];
     }]];

    [section addItem:[RETableViewItem itemWithTitle:@"地址管理" accessoryType:UITableViewCellAccessoryDisclosureIndicator selectionHandler:^(RETableViewItem *item) {
         AddressVC * CTL =  [[AddressVC alloc]init];
         [self.navigationController pushViewController:CTL animated:YES];
     }]];
    
    [section addItem:[RETableViewItem itemWithTitle:@"收银台" accessoryType:UITableViewCellAccessoryDisclosureIndicator selectionHandler:^(RETableViewItem *item) {
         CashVC * CTL =  (CashVC *)[SceneMap CashScene];
         [self.navigationController pushViewController:CTL animated:YES];
     }]];
    
    
    [section addItem:[RETableViewItem itemWithTitle:@"订单填写" accessoryType:UITableViewCellAccessoryDisclosureIndicator selectionHandler:^(RETableViewItem *item) {
        OrderEidtVC * CTL =  [[OrderEidtVC alloc]init];
        [self.navigationController pushViewController:CTL animated:YES];
     }]];
    
    [section addItem:[RETableViewItem itemWithTitle:@"回收功能首页" accessoryType:UITableViewCellAccessoryDisclosureIndicator selectionHandler:^(RETableViewItem *item) {
        RecyclingVC * CTL =  [[RecyclingVC alloc]init];
        [self.navigationController pushViewController:CTL animated:YES];
     }]];
    
    [section addItem:[RETableViewItem itemWithTitle:@"回收功能二级分类页" accessoryType:UITableViewCellAccessoryDisclosureIndicator selectionHandler:^(RETableViewItem *item) {
        RecyclingSecondVC * CTL =  [[RecyclingSecondVC alloc]init];
        [self.navigationController pushViewController:CTL animated:YES];
     }]];
    
    [section addItem:[RETableViewItem itemWithTitle:@"回收 估价页" accessoryType:UITableViewCellAccessoryDisclosureIndicator selectionHandler:^(RETableViewItem *item) {
        ValuationVC * CTL =  [[ValuationVC alloc]init];
        [self.navigationController pushViewController:CTL animated:YES];
     }]];
    
    
    [section addItem:[RETableViewItem itemWithTitle:@"个人中心" accessoryType:UITableViewCellAccessoryDisclosureIndicator selectionHandler:^(RETableViewItem *item) {
         PersonalCenterVC * CTL = (PersonalCenterVC *) [SceneMap PersonalScene];
         [self.navigationController pushViewController:CTL animated:YES];
     }]];
    
    
    

    [section addItem:[RETableViewItem itemWithTitle:@"二手" accessoryType:UITableViewCellAccessoryDisclosureIndicator selectionHandler:^(RETableViewItem *item) {
        ZTtabbarCtl * CTL = (ZTtabbarCtl *) [SceneMap ZTtabbarCtlScene];
        [self.navigationController pushViewController:CTL animated:YES];
    }]];
    
    
    [section addItem:[RETableViewItem itemWithTitle:@"我的" accessoryType:UITableViewCellAccessoryDisclosureIndicator selectionHandler:^(RETableViewItem *item) {
        ZTtabbarCtl * CTL = (ZTtabbarCtl *) [SceneMap ZTtabbarCtlScene];
        [self.navigationController pushViewController:CTL animated:YES];
    }]];
    


}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
}





@end


// ----------------------- 摇一摇功能 ------------------

#define test = 1;
#ifdef test

#define AtAspect Shake

#define AtAspectOfClass UIViewController
@classPatchField(UIViewController)

@synthesizeNucleusPatch(SuperCaller, -, void, motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event);

AspectPatch(-, void, motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event) {
//    UIViewController *rootController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    
    UINavigationController *rootViewController = [[UINavigationController alloc]initWithRootViewController: [[TestViewController alloc] initWithStyle:UITableViewStyleGrouped]];
    [URLNavigation setRootViewController:rootViewController];

}

@end

#undef AtAspectOfClass
#undef AtAspect

#endif
