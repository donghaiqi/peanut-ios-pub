//
//  EDUBaseViewController.m
//  Students
//
//  Created by xuchangyuan on 15/5/16.
//  Copyright (c) 2015年 xuchangyuan. All rights reserved.
//

#import "ZTBaseViewController.h"
#import <objc/runtime.h>
//#import <UMAnalytics/MobClick.h>


#define BAR_BUTTON_FONT       [UIFont systemFontOfSize:16.]
#define BAR_MAIN_TITLE_FONT   [UIFont boldSystemFontOfSize:16.]
#define BAR_SUB_TITLE_FONT    [UIFont systemFontOfSize:14.]
#define BAR_TITLE_PADDING_TOP -3.
#define BAR_TITLE_MAX_WIDTH   200

@interface ZTBaseViewController()<UIGestureRecognizerDelegate>

@end

static char buttonActionBlockKey;

@implementation ZTBaseViewController

-(ZTNavView *)navView{
    if (!_navView) {
        _navView = [[ZTNavView alloc] init];
    }
    return _navView;
}

-(UIView *)contentView{
    if (!_contentView) {
        _contentView = [[UIView alloc] init];
    }
    return _contentView;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    self.view.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.view.backgroundColor = [UIColor whiteColor];
    if (!self.navView.hidden) {
        self.navView.backgroundColor = RGBA(51, 51, 51, 1);
        [self.view addSubview:self.navView];
        [self.navView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.equalTo(self.view);
            make.height.mas_equalTo(kNavHeight);
        }];
        self.navView.frame = CGRectMake(0, 0, currentDeviceWidth, kNavHeight);
    }
    
    [self.view addSubview:self.contentView];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.navView.mas_bottom);
    }];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
//    [MobClick beginLogPageView:[[self class] description]];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
//    [MobClick endLogPageView:[[self class] description]];
}

#ifdef __IPHONE_7_0
- (UIRectEdge)edgesForExtendedLayout {
    return UIRectEdgeNone;
}
#endif

- (UIButton *)leftBarButtonWithTitle:(NSString *)title
                         barNorImage:(UIImage *)norImage
                         barHltImage:(UIImage *)hltImage
                              action:(void(^)(void))actionBlock{
    self.leftButton = [self buttonWithTitle:title buttonNorImage:norImage buttonHltImage:hltImage];
    objc_setAssociatedObject(self.leftButton, &buttonActionBlockKey, actionBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.leftButton];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    if (title && norImage) {
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                           initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                           target:nil action:nil];
        negativeSpacer.width = -10;
        self.navigationItem.leftBarButtonItems = @[negativeSpacer,barButtonItem];
    }
    
    return self.leftButton;
}

- (UIButton *)rightBarButtonWithTitle:(NSString *)title
                          barNorImage:(UIImage *)norImage
                          barHltImage:(UIImage *)hltImage
                               action:(void(^)(void))actionBlock{
    self.rightButton = [self buttonWithTitle:title buttonNorImage:norImage buttonHltImage:hltImage];
    objc_setAssociatedObject(self.rightButton, &buttonActionBlockKey, actionBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.rightButton];
    self.navigationItem.rightBarButtonItem = barButtonItem;
    return self.rightButton;
}

- (UIButton *)buttonWithTitle:(NSString *)title buttonNorImage:(UIImage *)norImage buttonHltImage:(UIImage *)hltImage{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    [button setImage:norImage forState:UIControlStateNormal];
    [button setImage:hltImage forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setTitleColor:RGBA(230, 230, 230, 0.5) forState:UIControlStateHighlighted];
    [button addTarget:self action:@selector(actionButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    button.backgroundColor = [UIColor clearColor];
    button.titleLabel.font = BAR_BUTTON_FONT;
    [button sizeToFit];
    return button;
}

- (void)actionButtonClicked:(UIButton *)sender{
    if ([sender isEqual:self.leftButton] && self.navigationController.viewControllers.count > 1) {
        if (sender.titleLabel.text.length > 0 || sender.imageView.image) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    void (^actionBlock) (void) = objc_getAssociatedObject(sender, &buttonActionBlockKey);
    if (actionBlock)actionBlock();
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

//#pragma mark - 屏幕旋转
//- (BOOL)shouldAutorotate
//{
//    return NO;
//}
//
//- (UIInterfaceOrientationMask)supportedInterfaceOrientations
//{
//    return UIInterfaceOrientationMaskPortrait;
//}
//
//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
//{
//    return UIInterfaceOrientationPortrait;
//}
//
//#pragma mark - 状态栏
//
//// 解决屏幕横屏时，状态栏隐藏
//- (BOOL)prefersStatusBarHidden
//{
//    return NO;
//}

@end
