//
//  CityModel.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/5/3.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CityModel : NSObject
//    "id": 652901,
//     "letter": "A",
//     "level": 3,
//     "name": "阿克苏市",
//     "parentId": 652900,
//     "zipCode": "843000"

@property(nonatomic,assign)int id;
@property(nonatomic,copy)NSString *letter;
@property(nonatomic,assign)int level;
@property(nonatomic,copy)NSString *name;
@property(nonatomic,assign)int parentId;
@property(nonatomic,assign)int zipCode;

@end

@interface NewCityModel : NSObject
@property(nonatomic,copy)NSString *title;
@property(nonatomic,strong)NSMutableArray *cities;

@end

NS_ASSUME_NONNULL_END
