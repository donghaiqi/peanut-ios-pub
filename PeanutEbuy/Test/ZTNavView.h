//
//  XCYNavView.h
//  movice
//
//  Created by xuchangyuan on 2019/6/11.
//  Copyright © 2019年 xuchangyuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZTNavView : UIView

@property(nonatomic,retain)UIButton *leftBtn;
@property(nonatomic,retain)UIButton *rightBtn;
@property(nonatomic,retain)UILabel *titleLabel;

-(UILabel *)titleLabelWithTitle:(NSString *)title action:(void(^)(void))actionBlock;

- (UIButton *)leftBarButtonWithTitle:(NSString *)title
                         barNorImage:(UIImage *)norImage
                         barHltImage:(UIImage *)hltImage
                              action:(void(^)(void))actionBlock;

- (UIButton *)rightBarButtonWithTitle:(NSString *)title
                          barNorImage:(UIImage *)norImage
                          barHltImage:(UIImage *)hltImage
                               action:(void(^)(void))actionBlock;

@end
