//
//  XCYNavView.m
//  movice
//
//  Created by xuchangyuan on 2019/6/11.
//  Copyright © 2019年 xuchangyuan. All rights reserved.
//

#import "ZTNavView.h"
#import <objc/runtime.h>

static char bthBlockKey;

@implementation ZTNavView

- (UIButton *)leftBarButtonWithTitle:(NSString *)title
                         barNorImage:(UIImage *)norImage
                         barHltImage:(UIImage *)hltImage
                              action:(void(^)(void))actionBlock{
    self.leftBtn = [self buttonWithTitle:title buttonNorImage:norImage buttonHltImage:hltImage];
    objc_setAssociatedObject(self.leftBtn, &bthBlockKey, actionBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    [self addSubview:self.leftBtn];
//    [self.leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.equalTo(self);
//        make.left.mas_equalTo(self.mas_left).and.offset(5);
//        make.size.mas_equalTo(CGSizeMake(44, 44));
//    }];
    self.leftBtn.frame = CGRectMake(5, viewBottom(self)-44, 44, 44);
    return self.leftBtn;
}

- (UIButton *)rightBarButtonWithTitle:(NSString *)title
                          barNorImage:(UIImage *)norImage
                          barHltImage:(UIImage *)hltImage
                               action:(void(^)(void))actionBlock{
    self.rightBtn = [self buttonWithTitle:title buttonNorImage:norImage buttonHltImage:hltImage];
    objc_setAssociatedObject(self.rightBtn, &bthBlockKey, actionBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    [self addSubview:self.rightBtn];
//    [self.rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(self.mas_right).and.offset(-5);
//        make.bottom.equalTo(self);
//        make.size.mas_equalTo(CGSizeMake(44, 44));
//    }];
    self.rightBtn.frame = CGRectMake(currentDeviceWidth - 49, viewBottom(self)-44, 44, 44);
    return self.rightBtn;
}

- (void)actionButtonClicked:(UIButton *)sender{
    void (^actionBlock) (void) = objc_getAssociatedObject(sender, &bthBlockKey);
    if (actionBlock)actionBlock();
}

- (UIButton *)buttonWithTitle:(NSString *)title buttonNorImage:(UIImage *)norImage buttonHltImage:(UIImage *)hltImage{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    [button setImage:norImage forState:UIControlStateNormal];
    [button setImage:hltImage forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = UIFont(17.);
//    [button setTitleColor:UIRGB(230, 230, 230, 0.5) forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [button addTarget:self action:@selector(actionButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    button.backgroundColor = [UIColor clearColor];
//    button.titleLabel.font = BAR_BUTTON_FONT;
    [button sizeToFit];
    return button;
}

-(UILabel *)titleLabelWithTitle:(NSString *)title action:(void(^)(void))actionBlock{
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.text = title;
//    self.titleLabel.backgroundColor = [UIColor blueColor];
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.userInteractionEnabled = YES;
    [self addSubview:self.titleLabel];
    float disW = 0.;
    float x = 0.;
    if(self.titleLabel.text.length == 0){
        
    }
    if (self.leftBtn && self.rightBtn && self.titleLabel.text.length == 0) {
        disW = 98.;
    }else if((self.leftBtn || self.rightBtn) && self.titleLabel.text.length == 0){
        disW = 49.;
    }
    self.titleLabel.frame = CGRectMake(x, viewBottom(self)-44, currentDeviceWidth-disW, 44);
//    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.leftBtn != nil ? self.leftBtn.mas_right : self.mas_left);
//        make.height.equalTo(@44);
//        make.right.equalTo(self.rightBtn != nil ? self.rightBtn.mas_left : self.mas_right);
//        make.bottom.equalTo(self);
//    }];
    return self.titleLabel;
}

@end
