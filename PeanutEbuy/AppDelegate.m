//
//  AppDelegate.m
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/3/3.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "TestViewController.h"
#import "SceneMap.h"
#import "LoginViewController.h"
//#import "OCRSDK.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [self showMainUI];
//    [OCRSDK initOCRWithAccount:@"zjyljk" andSecretKey:@"4FU7FE6C680032954RTF3EBADBDEC6YH"];

    return YES;
}

#pragma mark  public metheds
- (void)showMainUI {
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

#ifdef DEBUG
    UINavigationController *rootViewController = [[UINavigationController alloc]initWithRootViewController: [[TestViewController alloc] initWithStyle:UITableViewStyleGrouped]];
    
#else
    
    LoginViewController * CTL = (LoginViewController *) [SceneMap LoginScene];
    UINavigationController *rootViewController = [[UINavigationController alloc]initWithRootViewController:CTL];
#endif
    
    self.window.rootViewController = rootViewController;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
}


//#pragma mark - UISceneSession lifecycle
//
//
//- (UISceneConfiguration *)application:(UIApplication *)application configurationForConnectingSceneSession:(UISceneSession *)connectingSceneSession options:(UISceneConnectionOptions *)options {
//    // Called when a new scene session is being created.
//    // Use this method to select a configuration to create the new scene with.
//    return [[UISceneConfiguration alloc] initWithName:@"Default Configuration" sessionRole:connectingSceneSession.role];
//}
//
//
//- (void)application:(UIApplication *)application didDiscardSceneSessions:(NSSet<UISceneSession *> *)sceneSessions {
//    // Called when the user discards a scene session.
//    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
//    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
//}


@end
