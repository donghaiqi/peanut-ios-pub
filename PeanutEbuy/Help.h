//
//  Help.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/3/16.
//  Copyright © 2020 赵甜. All rights reserved.
//

#ifndef Help_h
#define Help_h

#define kScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define kScreenHeight ([UIScreen mainScreen].bounds.size.height)

#define WIDTH  [UIScreen mainScreen].bounds.size.width

#define HEIGHT [UIScreen mainScreen].bounds.size.height

#define BgColor [UIColor colorWithRed:244/255.f green:80/255.f blue:69/255.f alpha:1]

//iPhoneX / iPhoneXS
#define  isIphoneX_XS     (kScreenWidth == 375.f && kScreenHeight == 812.f ? YES : NO)
//iPhoneXR / iPhoneXSMax
#define  isIphoneXR_XSMax    (kScreenWidth == 414.f && kScreenHeight == 896.f ? YES : NO)
//异性全面屏
#define   XCYisFullScreen    (isIphoneX_XS || isIphoneXR_XSMax)

// Status bar height.
#define  StatusBarHeight     (XCYisFullScreen ? 34.f : 20.f)

#define kNavHeight (StatusBarHeight + 44)


#define IOS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]
#define iPhoneXx ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)
#define WeakSelf __weak typeof(self) weakSelf = self;

#define kNavigationViewH  (iPhoneXx?88:64)
#define kStatusHeight  (iPhoneXx?44:20)
#define kTabBarHeight  (iPhoneXx?83:49)
#define kSafeAreaBottom (iPhoneXx?34:0)


#define kDeviceiPhoneX_XS_XR_XSMax ((([UIScreen mainScreen].bounds.size.height == 812) && ([UIScreen mainScreen].bounds.size.width == 375))\
|| (([UIScreen mainScreen].bounds.size.height == 896) && ([UIScreen mainScreen].bounds.size.width == 414)))

#define viewBottom(view) (view.frame.origin.y + view.frame.size.height)
#define viewRight(view) (view.frame.origin.x + view.frame.size.width)

#define currentDeviceWidth ([UIScreen mainScreen].bounds.size.width)
#define currentDeviceHeight ([UIScreen mainScreen].bounds.size.height)

#define UIFont(__size__)        [UIFont systemFontOfSize:__size__]
#define UIBoldFont(__size__) [UIFont boldSystemFontOfSize:__size__]


#endif /* Help_h */
