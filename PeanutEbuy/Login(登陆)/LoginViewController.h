//
//  LoginViewController.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/3/10.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *userPhone_TF;
@property (weak, nonatomic) IBOutlet UITextField *passCode_TF;
@property (weak, nonatomic) IBOutlet UIButton *getCode_Btn;
@property (weak, nonatomic) IBOutlet UIButton *login_Btn;
- (IBAction)GetCodeClick:(id)sender;
- (IBAction)LoginBtnClick:(id)sender;

@end

NS_ASSUME_NONNULL_END
