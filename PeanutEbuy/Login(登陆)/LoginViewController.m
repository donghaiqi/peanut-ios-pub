//
//  LoginViewController.m
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/3/10.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import "LoginViewController.h"
#import "ZTtabbarCtl.h"


@interface LoginViewController ()<UITextFieldDelegate>{
    NSTimer *_timer;
    NSUInteger _timeInterval;
}

@end

@implementation LoginViewController

-(void)loadView{
    [super loadView];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title =@"手机验证码登录";
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backGround_img"]];
    _passCode_TF.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)dealloc{
    
}

- (IBAction)GetCodeClick:(id)sender {
    [_userPhone_TF resignFirstResponder];
    if ([Tools isValidateMobile:_userPhone_TF.text]) {
          if (![Tools JudgeTheillegalCharacter:_userPhone_TF.text]) {
        //        [self getCodeRequest];//请求验证码
                [self.getCode_Btn setTitleColor:[UIColor mainTextGrayColor] forState:UIControlStateNormal];
                self.getCode_Btn.enabled = NO;
                _timeInterval = 60;
                [self _startRefreshButtonState];
            }else{
                [self showFailWithText:@"包含非法字符请重新输入" yOffset:1];
            }
    }else{
        [self showFailWithText:@"请检查手机号码是否正确" yOffset:1];
    }
}

- (void)_startRefreshButtonState{
    [self.getCode_Btn setTitle:[NSString stringWithFormat:@"(%lu)s重发",(unsigned long)_timeInterval] forState:UIControlStateNormal];
    //    [self _stopTimer];
    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(_timerAction) userInfo:nil repeats:YES];
}

- (void)_timerAction {
    _timeInterval --;
    if (_timeInterval > 0){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.getCode_Btn setTitle:[NSString stringWithFormat:@"(%lu)s重发",(unsigned long)self->_timeInterval] forState:UIControlStateNormal];
        });
    } else {
        self.getCode_Btn.enabled = YES;
        [self.getCode_Btn setTitleColor:[UIColor mainBlueColor] forState:UIControlStateNormal];
        [self _stopTimer];
        [self.getCode_Btn setTitle:[NSString stringWithFormat:@"%@",@"获取验证码"] forState:UIControlStateNormal];
    }
}

- (void)_stopTimer{
    
    self.getCode_Btn.userInteractionEnabled = YES;
    [_timer invalidate];
    _timer = nil;
}


#pragma mark- 跳转首页
- (IBAction)LoginBtnClick:(id)sender {
    if (_userPhone_TF.text != nil) {
        if ([Tools isValidateMobile:_userPhone_TF.text]) {
            if (![Tools JudgeTheillegalCharacter:_userPhone_TF.text]) {
                if ([_passCode_TF.text isNotEmpty]) {
                    if (_passCode_TF.text.length<6) {
                        [self showFailWithText:@"请输入6位验证码" yOffset:1];
                    }else{
                        [self loginInRequest];
                        ZTLog(@"测试测试")
                        ZTtabbarCtl * CTL = (ZTtabbarCtl *) [SceneMap ZTtabbarCtlScene];
                        [URLNavigation setRootViewController:CTL];
                    }
                }else{
                    [self showFailWithText:@"短信验证码不能为空" yOffset:1];
                }
            }else{
                //非法字符
                [self showFailWithText:@"请检查手机号码格式是否正确" yOffset:1];
            }
        }else{
            [self showFailWithText:@"请检查手机号码格式是否正确" yOffset:1];
        }
    }else{
        [self showFailWithText:@"手机号码不能为空" yOffset:1];
    }
}

#pragma mark - textField代理方法
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if ([textField isEqual:_passCode_TF]) {
        if (_userPhone_TF.text.length == 0) {
            [self showMessage:@"手机号码不能为空" yOffset:1];
            return NO;
        }else{
            return YES;
        }
    }else{
        return YES;
    }
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSInteger existedLength = textField.text.length;
    NSInteger selectedLength = range.length;
    NSInteger replaceLength = string.length;
    NSInteger pointLength = existedLength - selectedLength + replaceLength;
    if ([textField isEqual:_passCode_TF]) {
        //不允许超过11位
        if (pointLength > 6) {
            [self showMessage:@"验证码只有6位" yOffset:1];
            return NO;
        }
        return YES;
    }
    return NO;
}



#pragma mark- 获取验证码接口
-(void)getCodeRequest{
    [ZTHttp getCodeByPhoneNum:_userPhone_TF.text success:^(NSDictionary *dic) {
          if (dic) {
              ZTLog(@"测试成功");
          }
      } failure_Nonnull:^(NSDictionary *dic) {
          ZTLog(@"测试");
      }];
}

#pragma mark- 登陆接口
-(void)loginInRequest{
    NSString * token =@"BTK-0d44b7dd-3ef2-4c06-87d7-f8f5605b5aa7";
    [ZTCore saveUsertoken:token];
    
//    [ZTHttp getMobileToken:_userPhone_TF.text smsCode:_passCode_TF.text success:^(NSDictionary * _Nullable dic) {
//
//    } failure_Nonnull:^(NSDictionary * _Nonnull dic) {
//
//    }];
}

@end
