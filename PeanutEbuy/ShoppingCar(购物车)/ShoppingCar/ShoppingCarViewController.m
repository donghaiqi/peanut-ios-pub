//
//  ShoppingCarViewController.m
//  demos
//
//  Created by 朱伟阁 on 2019/2/16.
//  Copyright © 2019 朱伟阁. All rights reserved.
//

#import "ShoppingCarViewController.h"
#import "ShoppingCarModel.h"
#import "ShoppingCarCell.h"
#import "ShoppingCarView.h"
#import "StoreCell.h"
//#import "UIViewController+ExtendCtr.h"

@interface ShoppingCarViewController ()<UITableViewDataSource,UITableViewDelegate,ShoppingCarViewDelegate,ShoppingCarCellDelegate>

@property(nonatomic, strong) dispatch_block_t deleteSelectedCell;
/** 数据源*/
@property (nonatomic, strong) NSMutableArray *dataArr;
/** 记录选中的cell*/
@property (nonatomic,strong)NSMutableArray *selectedCellArr;

@property(nonatomic, strong) UITableView *tv;
@property(nonatomic, strong) ShoppingCarView *shoppingCarBottomView;

//是否全选
@property(nonatomic, assign) BOOL selectAll;

@end

@implementation ShoppingCarViewController


-(UITableView *)tv{
    if(!_tv){
        UITableViewStyle style;
        if (@available(iOS 13.0, *)) {
            style =UITableViewStyleInsetGrouped;
        } else {
            style = UITableViewStyleGrouped;
        }
        _tv = [[UITableView alloc]initWithFrame:CGRectMake(0, kNavigationViewH+30, kScreenWidth, kScreenHeight-kNavigationViewH-kSafeAreaBottom-30-48) style:style];
       _tv.backgroundColor = [UIColor mainBackgroundColor];
       _tv.separatorStyle = UITableViewCellSeparatorStyleNone;
       _tv.sectionHeaderHeight = 0;
       _tv.sectionFooterHeight = 15;
       _tv.delegate = self;
       _tv.dataSource = self;
       [_tv registerNib:[UINib nibWithNibName:@"ShoppingCarCell" bundle:nil] forCellReuseIdentifier:NSStringFromClass([ShoppingCarCell class])];
       [_tv registerNib:[UINib nibWithNibName:@"StoreCell" bundle:nil] forCellReuseIdentifier:NSStringFromClass([StoreCell class])];
       self.tv.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
       self.tv.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];

    }
    return _tv;
}



-(NSMutableArray *)dataArr{
    if(_dataArr==nil){
        NSString *pathStr = [[NSBundle mainBundle]pathForResource:@"ShopCarData.plist" ofType:nil];
        NSArray *array = [NSMutableArray arrayWithContentsOfFile:pathStr];
        NSMutableArray *mutableArr = [NSMutableArray array];
        for (NSDictionary *groupDict in array) {
            ShoppingCarGroupModel *groupModel = [ShoppingCarGroupModel mj_objectWithKeyValues:groupDict];
            [mutableArr addObject:groupModel];
        }
        _dataArr = [mutableArr mutableCopy];
    }
    return _dataArr;
}

- (NSMutableArray *)selectedCellArr{
    if(_selectedCellArr==nil){
        _selectedCellArr = [NSMutableArray array];
    }
    return _selectedCellArr;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor mainBackgroundColor];
    self.navigationItem.title = @"购物车";
    
    [self addGoodsNumLabel];
    
    [self.view addSubview:self.tv];
    [self.view addSubview:self.shoppingCarBottomView];
    
    
//    _shoppingCarBottomView = [[ShoppingCarView alloc]initWithFrame:CGRectMake(0, _tv.frame.origin.y, kScreenWidth, 48)];
//    _shoppingCarBottomView = [[ShoppingCarView alloc]initWithFrame:CGRectMake(0, kScreenHeight-kSafeAreaBottom-48, kScreenWidth, 48)];
////    CGRectMake(0, kScreenHeight-kSafeAreaBottom-44, kScreenWidth, 44)
//          _shoppingCarBottomView.backgroundColor = [UIColor whiteColor];
//
//          _shoppingCarBottomView.delegate = self;
//    [self.view addSubview:self.shoppingCarBottomView];
}

- (ShoppingCarView *)shoppingCarBottomView{
    if(_shoppingCarBottomView == nil){
        _shoppingCarBottomView = [[ShoppingCarView alloc]initWithFrame:CGRectMake(0, kScreenHeight-kSafeAreaBottom-48, kScreenWidth, 48)];
        _shoppingCarBottomView.backgroundColor = [UIColor whiteColor];
        _shoppingCarBottomView.delegate = self;
    }
    return _shoppingCarBottomView;
}

-(void)addGoodsNumLabel{
    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(0, kNavigationViewH, self.view.width,30)];
    label.backgroundColor = [UIColor mainBackgroundColor];
    label.font  = [UIFont systemFontOfSize:13];
    label.textColor = [UIColor darkTextColor];
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
        paraStyle.alignment = NSTextAlignmentLeft;//对齐
        paraStyle.headIndent = 0.0f;//行首缩进
        //字体大小号字乘以2 即首行空出两个字符
        CGFloat emptylen = 38;
        paraStyle.firstLineHeadIndent = emptylen;//首行缩进
    //            paraStyle.tailIndent = 100.0f;//行尾缩进
    //            paraStyle.lineSpacing = 2.0f;//行间距
        NSAttributedString *attrText = [[NSAttributedString alloc] initWithString:@"测试共17件宝贝" attributes:@{NSParagraphStyleAttributeName:paraStyle}];
        label.attributedText = attrText;
    [self.view addSubview:label];
}



#pragma mark- delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    ShoppingCarGroupModel *groupModel = self.dataArr[section];
    return groupModel.detail.count+1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        return 40;
    } else {
        return 153;
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ShoppingCarGroupModel *groupModel = self.dataArr[indexPath.section];
    
    StoreCell *cellheader = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([StoreCell class])];
    if(!cellheader){
        cellheader = [[NSBundle mainBundle]loadNibNamed:@"StoreCell" owner:nil options:nil].firstObject;
    }
    ShoppingCarCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ShoppingCarCell class])];
    if(!cell){
        cell = [[NSBundle mainBundle]loadNibNamed:@"ShoppingCarCell" owner:nil options:nil].firstObject;
    }
    
    if (indexPath.row == 0) {
        [cellheader setInfo:groupModel];
        WeakSelf;
        cellheader.storeBlcok = ^(ShoppingCarGroupModel * _Nonnull groupModel) {
            [weakSelf storeBtnClick:groupModel];
        };
        [self judgeAllSelected];
        return cellheader;
    } else {
        ShoppingCarGoodsModel *goodsModel = groupModel.detail[indexPath.row-1];
        cell.section = indexPath.section;
        cell.row = indexPath.row;
        cell.delegate = self;
        [cell setInfo:goodsModel];
        return cell;
    }
    

}

- (void)calculatePrice{
    NSInteger sum = 0;
    NSInteger value = 0 ;
    for (ShoppingCarGoodsModel *goodsModel in self.selectedCellArr) {
        value = [goodsModel.CDprice integerValue]*[goodsModel.CDchooseCount integerValue];
        sum = sum+value;
    }
    if(sum > 0){
        self.shoppingCarBottomView.totalPrice.text = [NSString stringWithFormat:@"总价：%ld元",(long)sum];
    }else{
        self.shoppingCarBottomView.totalPrice.text = @"总价：";
    }
}

- (void)deviseOrAddReloadDataAndTableView{
    [self calculatePrice];
}

- (void)selectGoodsCell:(ShoppingCarCell *)cell{
    ShoppingCarGroupModel *groupModel = self.dataArr[cell.section];
    if(self.selectAll){
        groupModel.selectGroup = cell.goodsModel.selectGoods;
        [self.selectedCellArr removeObject:cell.goodsModel];
    }else{
        if(cell.goodsModel.selectGoods){
            [self.selectedCellArr addObject:cell.goodsModel];
            BOOL allSelected = YES;
            for (ShoppingCarGoodsModel *goodsModel in groupModel.detail) {
                if(!goodsModel.selectGoods){
                    allSelected = NO;
                }
            }
            groupModel.selectGroup = allSelected;
        }else{
            groupModel.selectGroup = NO;
            [self.selectedCellArr removeObject:cell.goodsModel];
        }
    }
    [self calculatePrice];
    [self.tv reloadData];
}

#pragma mark 判断全选
-(void)judgeAllSelected{
    //遍历每组确定是否全选  当逐条选中时，全部都选中的时候，全选选项也选中
       dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
           BOOL allSelected = YES;
           for (ShoppingCarGroupModel *groupModel in self.dataArr) {
               if (!groupModel.selectGroup) {
                   allSelected = NO;
               }
           }
           dispatch_async(dispatch_get_main_queue(), ^{
               if(allSelected){
                   [self.shoppingCarBottomView.selectALLBtn setImage:[UIImage imageNamed:@"color_choose"] forState:UIControlStateNormal];
                   self.shoppingCarBottomView.selectAll = YES;
                   self.selectAll = self.shoppingCarBottomView.selectAll;
               }else{
                   [self.shoppingCarBottomView.selectALLBtn setImage:[UIImage imageNamed:@"color_no_choose"] forState:UIControlStateNormal];
                   self.shoppingCarBottomView.selectAll = NO;
                   self.selectAll = self.shoppingCarBottomView.selectAll;
               }
           });
       });
}


- (void)storeBtnClick:(ShoppingCarGroupModel *)groupModel{
    if(self.selectAll){
        for (ShoppingCarGoodsModel *goodsModel in groupModel.detail) {
            goodsModel.selectGoods = NO;
            [self.selectedCellArr removeObject:goodsModel];
        }
    }else{
        if(groupModel.selectGroup){
            for (ShoppingCarGoodsModel *goodsModel in groupModel.detail) {
                goodsModel.selectGoods = groupModel.selectGroup;
                if([self.selectedCellArr containsObject:goodsModel]){
                    
                }else{
                   [self.selectedCellArr addObject:goodsModel];
                }
            }
        }else{
            for (ShoppingCarGoodsModel *goodsModel in groupModel.detail) {
                goodsModel.selectGoods = NO;
                [self.selectedCellArr removeObject:goodsModel];
            }
        }
    }
    [self calculatePrice];
    [self.tv reloadData];
}

- (void)deleteGoodsCell:(ShoppingCarCell *)cell{
    if(self.selectAll){
        [self.dataArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            ShoppingCarGroupModel *groupModel = (ShoppingCarGroupModel *)obj;
            [groupModel.detail enumerateObjectsUsingBlock:^(ShoppingCarGoodsModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if(obj == cell.goodsModel){
                    [self.selectedCellArr removeObject:obj];
                    [groupModel.detail removeObject:obj];
                    if(groupModel.detail.count == 0){
                        [self.dataArr removeObject:groupModel];
                    }else{
                        
                    }
                }
            }];
        }];
        if(!self.dataArr.count){
            [self.shoppingCarBottomView.selectALLBtn setImage:[UIImage imageNamed:@"color_no_choose"] forState:UIControlStateNormal];
            self.shoppingCarBottomView.selectALLBtn.enabled = NO;
        }
    }else{
        [self.dataArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            ShoppingCarGroupModel *groupModel = (ShoppingCarGroupModel *)obj;
            if(groupModel.selectGroup){
                [groupModel.detail enumerateObjectsUsingBlock:^(ShoppingCarGoodsModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if(obj == cell.goodsModel){
                        [self.selectedCellArr removeObject:obj];
                        [groupModel.detail removeObject:obj];
                        if(groupModel.detail.count == 0){
                            [self.dataArr removeObject:groupModel];
                        }else{
                            
                        }
                    }
                }];
            }else{
                [groupModel.detail enumerateObjectsUsingBlock:^(ShoppingCarGoodsModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if(obj == cell.goodsModel){
                        if(cell.goodsModel.selectGoods){
                            [self.selectedCellArr removeObject:obj];
                            [groupModel.detail removeObject:obj];
                            if(groupModel.detail.count == 0){
                                [self.dataArr removeObject:groupModel];
                            }else{
                                
                            }
                        }else{
                            [groupModel.detail removeObject:obj];
                            if(groupModel.detail.count == 0){
                                [self.dataArr removeObject:groupModel];
                            }else{
                                
                            }
                        }
                    }
                }];
            }
        }];
        if(!self.dataArr.count){
            [self.shoppingCarBottomView.selectALLBtn setImage:[UIImage imageNamed:@"color_no_choose"] forState:UIControlStateNormal];
            self.shoppingCarBottomView.selectALLBtn.enabled = NO;
        }
    }
    [self calculatePrice];
    [self.tv reloadData];
}

- (void)selectAllShopGoods:(BOOL)isSelectAll{
    self.selectAll = isSelectAll;
    if(self.selectedCellArr.count){
        [self.selectedCellArr removeAllObjects];
    }
    if(self.selectAll){
        for (ShoppingCarGroupModel *groupModel in self.dataArr) {
            groupModel.selectGroup = isSelectAll;
            for (ShoppingCarGoodsModel *goodsModel in groupModel.detail) {
                goodsModel.selectGoods = isSelectAll;
                [self.selectedCellArr addObject:goodsModel];
            }
        }
    }else{
        for (ShoppingCarGroupModel *groupModel in self.dataArr) {
            groupModel.selectGroup = NO;
            for (ShoppingCarGoodsModel *goodsModel in groupModel.detail) {
                goodsModel.selectGoods = NO;
            }
        }
    }
    [self calculatePrice];
    [self.tv reloadData];
}

//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
//    CustomFooterView *footerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([CustomFooterView class])];
//    if(!footerView){
//        footerView = [[CustomFooterView alloc]initWithReuseIdentifier:NSStringFromClass([CustomFooterView class])];
//    }
//    return footerView;
//}

@end
