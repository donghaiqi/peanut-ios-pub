//
//  StoreCell.m
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/3/27.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import "StoreCell.h"
#import "ShoppingCarModel.h"

@implementation StoreCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.sectionBtn setImage:[UIImage imageNamed:@"color_no_choose"] forState:UIControlStateNormal];
    [self.sectionBtn addTarget:self action:@selector(clickStoreSectionBtn) forControlEvents:UIControlEventTouchUpInside];
    
    self.shopNameLabel.textAlignment = NSTextAlignmentCenter;
    self.shopNameLabel.textColor = [UIColor blackColor];
    self.shopNameLabel.backgroundColor = [UIColor clearColor];
    self.shopNameLabel.layer.cornerRadius = 3;
    self.shopNameLabel.clipsToBounds = YES;
}



-(void)setListStyle:(NSString * _Nonnull)listStyle{
    if ([listStyle isEqualToString:@"List_By_Order"]) {
        
        [_sectionBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left);
            make.width.offset(0);
        }];
        _sectionBtn.hidden =YES;
    }
}


- (void)setInfo:(ShoppingCarGroupModel *)groupModel{
    self.groupModel = groupModel;
    if(groupModel.selectGroup){
        [self.sectionBtn setImage:[UIImage imageNamed:@"color_choose"] forState:UIControlStateNormal];
    }else{
        [self.sectionBtn setImage:[UIImage imageNamed:@"color_no_choose"] forState:UIControlStateNormal];
    }
    self.shopNameLabel.text = groupModel.name;
}


- (void)clickStoreSectionBtn{
    self.groupModel.selectGroup = !self.groupModel.selectGroup;
    if(self.groupModel.selectGroup){
        [self.sectionBtn setImage:[UIImage imageNamed:@"color_choose"] forState:UIControlStateNormal];
    }else{
        [self.sectionBtn setImage:[UIImage imageNamed:@"color_no_choose"] forState:UIControlStateNormal];
    }
    if (_storeBlcok) {
        self.storeBlcok(self.groupModel);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:NO];

    // Configure the view for the selected state
}

@end
