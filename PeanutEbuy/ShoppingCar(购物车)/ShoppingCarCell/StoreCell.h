//
//  StoreCell.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/3/27.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class ShoppingCarGroupModel;;

typedef void(^StoreSectionBtnBlock)(ShoppingCarGroupModel *groupModel);

@interface StoreCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *sectionBtn;

@property (weak, nonatomic) IBOutlet UILabel *shopNameLabel;

@property(nonatomic, strong) ShoppingCarGroupModel *groupModel;

@property(nonatomic, copy) StoreSectionBtnBlock storeBlcok;

@property(nonatomic, strong) NSString * listStyle;


-(void)setListStyle:(NSString * _Nonnull)listStyle;

- (void)setInfo:(ShoppingCarGroupModel *)groupModel;


@end

NS_ASSUME_NONNULL_END
