//
//  PersonalCenterVC.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/4/15.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import "ZTScene.h"

NS_ASSUME_NONNULL_BEGIN

@interface PersonalCenterVC : ZTScene
@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *view2;
@property (weak, nonatomic) IBOutlet UIView *view3;

@end

NS_ASSUME_NONNULL_END
