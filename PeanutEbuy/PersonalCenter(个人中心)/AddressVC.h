//
//  AddressVC.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/4/7.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddressVC : ZTScene

@property(nonatomic,strong)UITableView *tableView;

@end

NS_ASSUME_NONNULL_END
