//
//  AddressCell.m
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/4/7.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import "AddressCell.h"

@implementation AddressCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [_adressLb setTextColor:RGB(153, 153, 153)];
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:NO];

    // Configure the view for the selected state
}

- (IBAction)clickAdressBtn:(id)sender {
        self.ChooseBtnBlock();
}
- (IBAction)clickEditBtn:(id)sender {
    self.EditBtnBlock();
}
- (IBAction)clickDeleteBtn:(id)sender {
    self.DeleteBtnBlock();
}
@end
