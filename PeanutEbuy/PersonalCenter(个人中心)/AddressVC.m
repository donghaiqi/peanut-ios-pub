//
//  AddressVC.m
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/4/7.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import "AddressVC.h"
#import "AddressCell.h"

@interface AddressVC ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,assign) int selectIndex;//当前选中

@end

@implementation AddressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"地址管理";
    _selectIndex =3;
    [self resetUI];

}

-(void) resetUI{
    UITableViewStyle style;
      if (@available(iOS 13.0, *)) {
          style =UITableViewStyleInsetGrouped;
      } else {
          style = UITableViewStyleGrouped;
      }
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height-47) style:UITableViewStylePlain];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate =self;
    self.tableView.dataSource =self;
    self.tableView.backgroundColor = [UIColor mainBackgroundColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:@"AddressCell" bundle:nil] forCellReuseIdentifier:NSStringFromClass([AddressCell class])];
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
    [self.view addSubview:_tableView];

    
    UIButton * addAdressBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, self.tableView.frame.size.height, self.view.width, 47)];
    [addAdressBtn setTitle:@"新增地址" forState:UIControlStateNormal];
    addAdressBtn.backgroundColor = RGBA(51, 152, 218, 1);
    addAdressBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0);
    [addAdressBtn setImage:[UIImage imageNamed:@"add_adress_img"] forState:UIControlStateNormal];
    [addAdressBtn setImage:[UIImage imageNamed:@"add_adress_img"] forState:UIControlStateSelected];
    [addAdressBtn addTarget:self action:@selector(clickAddBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:addAdressBtn];
}

-(void)clickAddBtn{
    [self showMessage:@"添加地址" yOffset:1];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 105;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.width, 10)];
    view.backgroundColor =[UIColor mainBackgroundColor];
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AddressCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(AddressCell.class) forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if ((int)indexPath.section == _selectIndex) {
        [cell.adressBtn setImage:[UIImage imageNamed:@"choose_img"] forState:UIControlStateNormal];
    }else{
        [cell.adressBtn setImage:[UIImage imageNamed:@"no_choose_img"] forState:UIControlStateNormal];
    }
    
    WeakSelf;
    cell.ChooseBtnBlock = ^{
        weakSelf.selectIndex = (int)indexPath.section;
        [weakSelf.tableView reloadData];
    };
    
    cell.EditBtnBlock = ^{
        [weakSelf showMessage:@"跳转编辑地址" yOffset:1];
    };
    
    cell.DeleteBtnBlock = ^{
        [weakSelf showMessage:@"删除地址" yOffset:1];
    };
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
