//
//  PersonalCenterVC.m
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/4/15.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import "PersonalCenterVC.h"

@interface PersonalCenterVC ()

@end

@implementation PersonalCenterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setStatusBar];
    
    self.view.backgroundColor = [UIColor mainBackgroundColor];
    
    [_view1.layer setCornerRadius:6];
    [_view1.layer setMasksToBounds:YES];
    [_view2.layer setCornerRadius:6];
    [_view2.layer setMasksToBounds:YES];
    [_view3.layer setCornerRadius:6];
    [_view3.layer setMasksToBounds:YES];
    

}

-(void)setStatusBar{
     if (@available(iOS 13.0, *)) {
            [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;//文字是黑色
    } else {
            [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;//文字黑色
    }
    [self.navigationController.navigationBar setHidden:YES];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
