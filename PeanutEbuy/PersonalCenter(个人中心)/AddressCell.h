//
//  AddressCell.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/4/7.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^ChooseBtnBlock)(void);
typedef void(^EditBtnBlock)(void);
typedef void(^DeleteBtnBlock)(void);

@interface AddressCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameAndPhone;
@property (weak, nonatomic) IBOutlet UILabel *adressLb;
@property (weak, nonatomic) IBOutlet UIButton *adressBtn;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;

@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;


@property (copy, nonatomic)  ChooseBtnBlock ChooseBtnBlock;
@property (copy, nonatomic)  EditBtnBlock EditBtnBlock;
@property (copy, nonatomic)  DeleteBtnBlock DeleteBtnBlock;



- (IBAction)clickAdressBtn:(id)sender;
- (IBAction)clickEditBtn:(id)sender;
- (IBAction)clickDeleteBtn:(id)sender;

@end

NS_ASSUME_NONNULL_END
