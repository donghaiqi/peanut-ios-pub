#import "UIColor+AppColor.h"

@implementation UIColor (AppColor)


+ (UIColor *)mainColor {
    return RGB(4,26,48);//导航栏颜色
}

+(UIColor *)mainBackgroundColor{
    return RGBA(245, 245, 245, 1);
}


+(UIColor *)mainTextColor{
    return RGB(195, 239, 255);
}
//阴影文字颜色
+(UIColor *)mainPlaceholdColor{
    return RGBA(239, 239, 239, 1);
}

+(UIColor *)mainTextGrayColor{
    return RGBA(153, 153, 153, 1);
}

//按钮蓝色
+(UIColor *)mainBlueColor{
    return RGBA(51, 152, 218, 1);

}


+ (UIColor *)lineColor {
    return RGB(57, 67, 80);
}




@end
