//
//  SceneMap.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/3/4.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZTtabbarCtl.h"


NS_ASSUME_NONNULL_BEGIN

@interface SceneMap : NSObject

+ (UIViewController *)TestScene;

+ (UIViewController *)LoginScene;

+ (UIViewController *)ViewScene;

+ (ZTtabbarCtl *)ZTtabbarCtlScene;

+ (UIViewController *)ChooseCityCTLScene;

+ (UIViewController *)CashScene;

+ (UIViewController *)PersonalScene;


//+ (UIViewController *)ProductListCTLScene;


+ (UIViewController *)viewControllerFromStoryboard:(NSString *)storyboardName identifier:(NSString *)identifier;

+ (UIViewController *)viewControllerWithClass:(NSString *)className;

@end

NS_ASSUME_NONNULL_END
