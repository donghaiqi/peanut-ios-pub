/*
 * @brief:  全局配色设置
 * @author: Zq
 * @date:   2015-10-11
 */

#import <EasyIOS/UIColor+EasyExtend.h>

@interface UIColor (AppColor)


// 风格主要配色
+ (UIColor *)mainColor;
+ (UIColor *)mainBackgroundColor;
+(UIColor *)mainPlaceholdColor;//标签背景阴影
+(UIColor *)mainTextGrayColor;//文字灰色

+(UIColor *)mainBlueColor;//按钮蓝色
//+ (UIColor *)mainGrayColor;
//
//+(UIColor *)mainTextColor;
//
//+(UIColor *)mainPlaceholdColor;



@end
