#import "ZTScene.h"
#import "SceneMap.h"




@implementation ZTScene {
}



- (void)viewDidLoad {

    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
//    [self.view setBackgroundColor: [UIColor mainBackgroundColor]];
   // self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"CostomNormalBackImg"]] ;

    [super viewDidLoad];
    
//    if(IOS7_OR_LATER){
        self.automaticallyAdjustsScrollViewInsets = NO;
        self.extendedLayoutIncludesOpaqueBars = YES;
        self.edgesForExtendedLayout = UIRectEdgeNone;
//    }
    if (self.leftBtnShowBool == NO)
    {
        [self showBarButton:NAV_LEFT imageName:@"icon-back"];
    }

    self.view.backgroundColor = [UIColor mainBackgroundColor];
}
- (void)leftButtonTouch
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)setViewBackGroundImg{
    
     _imageVeiw= [[UIImageView alloc]initWithFrame:self.view.frame];
    [_imageVeiw setImage:[UIImage imageNamed:@"CostomNormalBackImg"]];
    [self.view addSubview:_imageVeiw];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    for (UIView * view in [self.view subviews]) {
        if ([view isKindOfClass:[UITextField class]]) {
            [self setTextFieldLeftPadding:(UITextField *)view forWidth:20];
        }
    }
}


- (void)addLeftNavBar{
    [self showBarButton:NAV_LEFT imageName:@"icon-back"];
}



-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}


- (void)HideBackGroundImg{
    [_imageVeiw removeFromSuperview];
}

#pragma mark - 警告窗口

//红色：R193 G59 B63 A0.8




#pragma mark 添加返回按钮
/**
 * @brief   添加返回按钮
 */

#pragma mark 私有方法
-(void)setTextFieldLeftPadding:(UITextField *)textField forWidth:(CGFloat)leftWidth
{
    CGRect frame = textField.frame;
    frame.size.width = leftWidth;
    UIView *leftview = [[UIView alloc] initWithFrame:frame];
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.leftView = leftview;
}





- (void)startRefresh {
//    [self.pushToBounceWrapper startLoading];

//    [UIView animateWithDuration:0.2f delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
//        self.pushToBounceWrapper.scrollView.contentOffset = CGPointMake(0, -[self.pushToBounceWrapper stopPos] - 1);
//    } completion:NULL];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
//
//    _pushToBounceWrapper.frame = _pushToBounceWrapper.scrollView.frame;
//    _appearCount++;
}

@end
