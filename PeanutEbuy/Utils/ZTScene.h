

/*
* @brief:  基类Scene, ViewController都继承这个
* @author: Hj
* @date:   2016-05-20
*/


#import "Scene.h"
@interface ZTScene : Scene


- (void)showFailWithText:(NSString *)text subTitle:(NSString *)subTitle;
//- (void)showMessageInViewControllerWithTitle:(NSString *)title subTitle:(NSString *)subTitle type:(TSMessageNotificationType)type;

- (void)stopRefreshAnimation;
- (void)startRefresh;
-(void)setTextFieldLeftPadding:(UITextField *)textField forWidth:(CGFloat)leftWidth;

-(void)setViewBackGroundImg;
- (void)HideBackGroundImg;


/**
 * @brief   添加返回按钮
 */
- (void)addLeftNavBar;

- (void)setupDropDownMenu;

- (void)showDropDownMenu;

-(void)click_waringBtn;


@property (nonatomic, assign) BOOL leftBtnShowBool;//判断是否修改默认返回按钮 yes 是 no不是

@property (nonatomic, strong) UIImageView * imageVeiw;

//@property (nonatomic, strong) UIButton * waringBtn;




@end
