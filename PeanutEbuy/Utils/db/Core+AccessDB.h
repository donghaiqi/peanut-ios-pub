//
//  Core+AccessDB.h
//  IntegrationMall
//
//  Created by 帅拓 on 16/8/18.
//  Copyright © 2016年 赵甜. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "UserInfo.h"
#define ZTCore      [Core_AccessDB sharedInstance]

@interface Core_AccessDB : NSObject
AS_SINGLETON(Core_AccessDB)


//保存当前用户token
- (void)saveUsertoken:(NSString*)usertoken;
//存取用户信息
- (NSString*)getUsertoken;

//存取用户信息
- (void)saveUserInfo:(UserInfo *)user_info;
- (UserInfo*)getUserInfo;
- (void)removeUserInfo;


//退出登录删除所有保存的表
- (void)logout;
//删除保存的数据
-(void)removeObjectForKey:(NSString *)key;



@end
