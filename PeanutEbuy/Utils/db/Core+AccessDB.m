     //
//  Core+AccessDB.m
//  IntegrationMall
//
//  Created by 帅拓 on 16/8/18.
//  Copyright © 2016年 赵甜. All rights reserved.
//

#import "Core+AccessDB.h"
#import "JPConfig.h"
@implementation Core_AccessDB
 DEF_SINGLETON(Core_AccessDB)


//保存当前token
- (void)saveUsertoken:(NSString*)usertoken{
    [JPConfigCurrent setObject:usertoken forKey:@"token"];
}
//存取用户token
- (NSString*)getUsertoken{
    NSString* userToken = [JPConfigCurrent objectForKey:@"token"];
    return userToken;
}


//存取用户信息
- (void)saveUserInfo:(UserInfo*)user_info{
    [JPConfigCurrent setObject:[user_info mj_JSONString] forKey:@"user_info"];
}

- (UserInfo*)getUserInfo{
    NSString* user_info = [JPConfigCurrent objectForKey:@"user_info"];
    if (user_info) {
        UserInfo * tempUserModel = [UserInfo mj_objectWithKeyValues:user_info];
        return tempUserModel;
    }
    return nil;
}

- (void)removeUserInfo{
    [JPConfigCurrent removeObjectForKey:@"user_info"];
}



//删除保存的数据
-(void)removeObjectForKey:(NSString *)key{
    [JPConfigCurrent removeObjectForKey:key];
}


- (void)logout{
    [self removeObjectForKey:@"user_info"];
}


//json To array
-(NSArray *)jsonToArray:(NSString *)jsonStr{
    NSArray *deserializedArray = [[NSArray alloc]init];
    NSError *error = nil;
    if (jsonStr) {
        id jsonObject = [NSJSONSerialization
                         JSONObjectWithData:[jsonStr mj_JSONData] options:NSJSONReadingAllowFragments
                         error:&error];
        if (!jsonObject) {
            ZTLog(@"jsonObject=============nil");
            return nil;
        }else if ([jsonObject isKindOfClass:[NSArray class]]){
            deserializedArray = (NSArray *)jsonObject;
        }
        
        return deserializedArray;
    }else{
        return nil;
    }
}


@end
