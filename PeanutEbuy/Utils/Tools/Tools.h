/*
 * @brief:  通用的工具函数
 * @author: Zq
 * @date:   2015-10-11
 */

@class BCGift;


@interface Tools : NSObject


+ (BOOL)isEmpty:(NSString *)string;

+ (void)shakeView:(UIView*)viewToShake;

+ (BOOL)isValidateMobile:(NSString *)mobile;

+ (BOOL)JudgeTheillegalCharacter:(NSString *)content;

+ (NSString *)getUUID;

+ (NSString *)getVersion;

+ (NSString *)getBuild;

+ (NSString *)getPhoneName;

+ (CGPoint)centerOfView:(UIView*)view;


#pragma mark 判断时间是否是在今天
+ (BOOL)isCurrentDay:(NSDate *)aDate;

+ (id)findResponder:(Class)dst selfs:(id)src;

+ (id)loadFirstNibWithName:(NSString *)name;


+ (void)showAlert:(NSString*)title message:(NSString*)message
cancelButtonTitle:(NSString*)cancelTitle cancelButtonBlock:(void(^)())cancelBlock
    doButtonTitle:(NSString*)doButtonTitle doButtonBlock:(void(^)())doButtonBlock;

+ (NSString*)formattedCoinString:(NSInteger)coin;

//获取游戏区域高度
+ (CGFloat)getGameAreaHeight;

//查找用户信息
+ (id)getContactPeopleModelByLocal:(id)data;

#pragma mark json 转字典
+(id)JSONWithData:(NSData *)data;

#pragma mark json 转字典
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;


#pragma mark 获取当前的网络
/*
 @description 获取当前的网络
 @return 0-无网络,1-WIFI  2-2G,3-3G,4-4G
 */
+ (int)getCurrentNetWork;
+ (BOOL)isHasNetWork;//网络连接状态

#pragma mark 时间转换相关

//获取当前时间的时间戳
+(NSString*)getCurrentTimestamp;


//*  字符串转NSDate
+ (NSDate *)dateFromString:(NSString *)timeStr
                    format:(NSString *)format;
//*  NSDate转字符串
+ (NSString *)datestrFromDate:(NSDate *)date
               withDateFormat:(NSString *)format;

+(NSDateFormatter *)defaultFormatter;

/**
 *  NSDate转时间戳
 *
 *  @param date 字符串时间
 *
 *  @return 返回时间戳
 */
+ (NSInteger)cTimestampFromDate:(NSDate *)date;
//时长
+ (NSString *)getTimeStrWithTimeInterval:(NSInteger)duration;
//字符串转时间戳
+(NSString *)cTimestampFromString:(NSString *)timeStr
                           format:(NSString *)format;
//将date时间戳转变成时间字符串
+ (NSString *)getDateStringWithDate:(NSString *)dateStr DateFormat:(NSString *)formatString;

//计算时间差
+(NSString * )getUserAge:(NSString *)str;


+(NSString *)cTimestampFromString:(NSString *)timeStr
                           format:(NSString *)format;

#pragma mark - 时间段问候

+(NSString *)getTheTimeBucket;


#pragma mark 生成随机数
+ (float)randomFloatFromMin:(float)min max:(float)max;

+ (CGSize)expectedSizeLabel:(UILabel*)label;

+ (id)random:(NSArray*)array;

#pragma mark 设置角标
+ (void)set_badget_number_with:(UITabBarItem*)item  andCount:(NSString *)_XGpushMsgNum;

#pragma mark 计算文件出大小
+ (NSString *)fileSizeWithInterge:(NSInteger)size;
//遍历文件夹获得文件夹大小，返回多少M
+ (float ) folderSizeAtPath:(NSString*) folderPath;

+ (long long)fileSizeOnePath:(NSString*) filePath;

#pragma mark 页面跳转
//获取当前主屏幕。
+(UIWindow*)find_top_window;


#pragma mark 判断字符串
+(BOOL)IsChinese:(NSString *)str;

//获取星座
+(NSString *)GetStartSeat:(NSString *)str;


#pragma mark 清空文件缓存。
+(void)CleanCacheAndLocalFile;




//判断设备是否IPHONE5
#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
//判断设备是否IPHONE6
#define iPhone6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)

@end
