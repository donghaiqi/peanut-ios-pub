//
//  ZTTools.h
//  STIM
//
//  Created by 帅拓 on 2016/12/13.
//  Copyright © 2016年 赵甜. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZTTools : NSObject
+ (BOOL)isEmpty:(NSString *)string;

+ (void)showAlert:(NSString *)message;

//通过文字长度计算高度，
+ (CGSize)sizeWithText:(NSString *)text font:(UIFont *)font maxSize:(CGSize)maxSize;

//等比缩放
+(UIImage *)thumbnailWithImageWithoutScale:(UIImage *)image size:(CGSize)asize;

//旋转图片
+ (UIImage *)fixOrientation:(UIImage *)aImage;

//字符串去除特殊字符
+(NSString *)removeSpecialChar:(NSString *)tempstr;

//字符串排序
+(NSString *)StringToSorting:(NSString *)tempstr;



//获取顶层控制器
+(UIViewController *)theTopviewControler;

+ (UIViewController *)getCurrentVC;

//星期转化
+(NSString * )weekDay_Eng_TO_Ch:(NSString *)Str;
//检查网络
+(BOOL)checkNetworkState;
@end
