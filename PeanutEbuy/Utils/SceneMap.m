//
//  SceneMap.m
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/3/4.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import "SceneMap.h"

@implementation SceneMap

+ (UIViewController *)TestScene{
      return [self viewControllerFromStoryboard:@"Main" identifier:@"TestViewController"];
}


#pragma mark 登录
+ (UIViewController *)LoginScene{
    return [self viewControllerFromStoryboard:@"Main" identifier:@"LoginViewController"];
}


+ (ZTtabbarCtl *)ZTtabbarCtlScene{
        return (ZTtabbarCtl *)[self viewControllerFromStoryboard:@"Main" identifier:@"ZTtabbarCtl"];
}


+ (UIViewController *)ChooseCityCTLScene{
    return (ZTtabbarCtl *)[self viewControllerFromStoryboard:@"Main" identifier:@"ChooseCityCTL"];
}

+ (UIViewController *)ViewScene{
    return (ZTtabbarCtl *)[self viewControllerFromStoryboard:@"Main" identifier:@"ViewController"];

}


#pragma mark 收银台
+ (UIViewController *)CashScene{
    return [self viewControllerFromStoryboard:@"Main" identifier:@"CashVC"];
}

#pragma mark 个人中心
+ (UIViewController *)PersonalScene{
    return [self viewControllerFromStoryboard:@"Main" identifier:@"PersonalCenterVC"];

}


//+ (UIViewController *)ProductListCTLScene{
//    return [self viewControllerFromStoryboard:@"Main" identifier:@"TestViewController"];
//
//}


+ (UIViewController *)viewControllerFromStoryboard:(NSString *)storyboardName identifier:(NSString *)identifier{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    if (identifier) {
        return [storyboard instantiateViewControllerWithIdentifier:identifier];
    }
    return [storyboard instantiateInitialViewController];
}

+ (UIViewController *)viewControllerWithClass:(NSString *)className {
    Class vcClass = NSClassFromString(className);
    return (UIViewController *)[[vcClass alloc] init];
}

@end
