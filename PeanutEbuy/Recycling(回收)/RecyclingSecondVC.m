//
//  RecyclingSecondVC.m
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/4/11.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import "RecyclingSecondVC.h"
#import "Header.h"
#import "ClassIficationCell.h"
#import "ClassCollectionCell.h"
#import "TYTabPagerBar.h"


@interface RecyclingSecondVC ()<UITableViewDataSource,UITableViewDelegate,TYTabPagerBarDataSource,TYTabPagerBarDelegate>
{


    BOOL _isRelate;
}
@property (nonatomic, strong) TYTabPagerBar *tabBar;
@property (nonatomic, assign) NSInteger firstIndex;

//@property (nonatomic, strong) NSMutableArray *datas;
@property (nonatomic, strong) NSMutableArray * brand_arr; //品牌
@property (nonatomic, strong) NSMutableArray *device_type_arr; //设备类型
@property (nonatomic, strong) UITableView * leftTableView;
@property (nonatomic, strong) UITableView * rightTableView;




@end

@implementation RecyclingSecondVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:@"闲置回收"];
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, nil];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.barTintColor = BgColor;
    
    //初始化数据源
    _brand_arr = [[NSMutableArray alloc]init];
    _device_type_arr = [[NSMutableArray alloc]init];
//     排序显示
    for (int a=0; a<_recycle_list.count; a++) {
        RecycleModel * model = [_recycle_list objectAtIndex:a];
        if (model.category_id == _model.category_id) {
            [_recycle_list removeObject:model];
            break;
        }
    }
    [_recycle_list insertObject:_model atIndex:0];

    //添加试图
    [self addPagerTabBar];
    [self creatLeftTableView];
    [self creatRightTableView];
    

    //top
    [_tabBar reloadData];
    
    //left
    [self get_Brand_List_Request:_model.category_id];
    
//     right
//    [_rightTableView reloadData];

}


//顶部pagerTaBar
- (void)addPagerTabBar {
    _tabBar = [[TYTabPagerBar alloc]initWithFrame:CGRectMake(0, 0, WIDTH,42)];
    _tabBar.layout.selectedTextColor = [UIColor blackColor];
    _tabBar.layout.progressColor = [UIColor mainBlueColor];
    _tabBar.layout.adjustContentCellsCenter = YES;
    _tabBar.layout.barStyle = TYPagerBarStyleProgressElasticView;
    _tabBar.layout.cellSpacing = 0;
    _tabBar.layout.cellEdging = 0;
    _tabBar.dataSource = self;
    _tabBar.delegate = self;
    _firstIndex = 0;
    [_tabBar scrollToItemAtIndex:0 atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
    [_tabBar registerClass:[TYTabPagerBarCell class] forCellWithReuseIdentifier:[TYTabPagerBarCell cellIdentifier]];
    [self.view addSubview:_tabBar];
}

-(UITableView *)creatLeftTableView{
    if (_leftTableView == nil) {
        _leftTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 42, WIDTH/4,HEIGHT-42) style:UITableViewStylePlain];
        _leftTableView.backgroundColor = [UIColor mainBackgroundColor];
        _leftTableView.delegate = self;
        _leftTableView.dataSource = self;
        _leftTableView.separatorStyle = NO;
        _leftTableView.rowHeight = 45;
        _leftTableView.scrollEnabled = NO;//不能滑动
        [_leftTableView registerClass:[ClassIficationCell class] forCellReuseIdentifier:NSStringFromClass([ClassIficationCell class])];
        
        [self.view addSubview:_leftTableView];
    }
    return _leftTableView;
}

-(UITableView *)creatRightTableView{
    if (_rightTableView == nil) {
        _rightTableView = [[UITableView alloc]initWithFrame:CGRectMake(WIDTH/4, 42, WIDTH*3/4, HEIGHT - 64-42) style:UITableViewStylePlain];
        _rightTableView.backgroundColor = [UIColor whiteColor];
        _rightTableView.delegate = self;
        _rightTableView.dataSource = self;
        _rightTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _rightTableView.rowHeight = 45;
        _rightTableView.scrollEnabled = YES;//不能滑动
        [_rightTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:NSStringFromClass([UITableViewCell class])];
        [_rightTableView setSeparatorInset:UIEdgeInsetsZero];
        [_rightTableView setLayoutMargins:UIEdgeInsetsZero];
//        NSIndexPath *firstPath = [NSIndexPath indexPathForRow:0 inSection:0];
//        [_rightTableView selectRowAtIndexPath:firstPath animated:NO scrollPosition:UITableViewScrollPositionTop];//默认选中一行
        [self.view addSubview:_rightTableView];
    }
    return _rightTableView;
}

#pragma mark- 接口
//获取品牌数组
-(void)get_Brand_List_Request:(int)cateId{

    WeakSelf;
    [ZTHttp recycle_brand_list:cateId success:^(NSDictionary * _Nullable dic) {
        if (dic) {
            weakSelf.brand_arr = [BrandModel mj_objectArrayWithKeyValuesArray:[dic objectForKey:@"data"]];
            [weakSelf.leftTableView reloadData];
            if (weakSelf.brand_arr.count >0) {
                NSIndexPath *firstPath = [NSIndexPath indexPathForRow:0 inSection:0];
                [weakSelf.leftTableView selectRowAtIndexPath:firstPath animated:NO scrollPosition:UITableViewScrollPositionTop];//默认选中一行
                BrandModel* tempModel = weakSelf.brand_arr[0];
                [weakSelf get_recycle_Type_Request:tempModel.brand_id];
            }
        }
    } failure_Nonnull:^(NSDictionary * _Nonnull dic) {
        
    }];
}

//查询型号列表
-(void)get_recycle_Type_Request:(int)brandId{
    WeakSelf;
    [ZTHttp recycle_model_list:_model.category_id brandId:brandId success:^(NSDictionary * _Nullable dic) {
        if (dic) {
            weakSelf.device_type_arr = [TypeModel mj_objectArrayWithKeyValuesArray:[dic objectForKey:@"data"]];
            [weakSelf.rightTableView reloadData];
            if (weakSelf.device_type_arr.count >0) {
                NSIndexPath *firstPath = [NSIndexPath indexPathForRow:0 inSection:0];
                [weakSelf.rightTableView selectRowAtIndexPath:firstPath animated:NO scrollPosition:UITableViewScrollPositionTop];//默认选中一行
            }
            ZTLog(@"回收设备型号请求成功");
        }
    } failure_Nonnull:^(NSDictionary * _Nonnull dic) {
        ZTLog(@"回收设备型号请求失败");
    }];
}

#pragma mark - TYTabPagerBarDataSource

- (NSInteger)numberOfItemsInPagerTabBar {
    return _recycle_list.count;
}

- (UICollectionViewCell<TYTabPagerBarCellProtocol> *)pagerTabBar:(TYTabPagerBar *)pagerTabBar cellForItemAtIndex:(NSInteger)index {
    UICollectionViewCell<TYTabPagerBarCellProtocol> *cell = [pagerTabBar dequeueReusableCellWithReuseIdentifier:[TYTabPagerBarCell cellIdentifier] forIndex:index];
    RecycleModel * model = [_recycle_list objectAtIndex:index];
    cell.titleLabel.text = model.name;
    return cell;
}

#pragma mark - TYTabPagerBarDelegate
- (CGFloat)pagerTabBar:(TYTabPagerBar *)pagerTabBar widthForItemAtIndex:(NSInteger)index {
    RecycleModel * model = [_recycle_list objectAtIndex:index];
    NSString *title = model.name;
    return [pagerTabBar cellWidthForTitle:title];
}

- (void)pagerTabBar:(TYTabPagerBar *)pagerTabBar didSelectItemAtIndex:(NSInteger)index {
    [pagerTabBar scrollToItemFromIndex:_firstIndex toIndex:index animate:YES];
    _firstIndex = index;
    _model = [_recycle_list objectAtIndex:index];
    [self get_Brand_List_Request:_model.category_id];
//    [self showMessage:@"准备刷新页面" yOffset:1];
}




#pragma mark- tableView delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;

//    if (tableView == _leftTableView){
//        return 1;
//    }else{
////        return _brand_arr.count;
//    }
}

// 行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == _leftTableView){
        return _brand_arr.count;
    }else{
        return _device_type_arr.count;
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _leftTableView){
        ClassIficationCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ClassIficationCell class])];
        if (cell == nil){
            cell = [[ClassIficationCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:NSStringFromClass([ClassIficationCell class])];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell addline];
        cell.textLabel.font = [UIFont systemFontOfSize:13];
        
        BrandModel* model = _brand_arr[indexPath.row];
        cell.textLabel.text = model.name;
        return cell;
    }else{
        UITableViewCell * rightCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class])];
        if (rightCell == nil) {
            rightCell = [[UITableViewCell alloc]init];
        }
        rightCell.selectionStyle = UITableViewCellSelectionStyleNone;
        rightCell.backgroundColor = [UIColor whiteColor];
        rightCell.textLabel.font = [UIFont systemFontOfSize:13];
        TypeModel * typeModel = [_device_type_arr objectAtIndex:indexPath.row];
        [rightCell.imageView sd_setImageWithURL:[NSURL URLWithString:typeModel.imgUrl] placeholderImage:[UIImage imageNamed:@"iphone_img"]];
        rightCell.textLabel.text = typeModel.name;
        return rightCell;
        
    }

    
}


// tableview cell 选中
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if (tableView == _leftTableView) {
        _isRelate = NO;
//        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//        cell.textLabel.textColor = RGB(51, 152, 218);
        
//        [_leftTableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionMiddle];
//        //将tableView的滑动范围调整到tableView相对应的cell的内容
//        [_rightTableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionTop];
        BrandModel * tempmodel = [_brand_arr objectAtIndex:indexPath.row];
        [self get_recycle_Type_Request:tempmodel.brand_id];
    }else{
        [self showMessage:@"跳转" yOffset:1];
    }
}

@end
