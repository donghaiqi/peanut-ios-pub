//
//  RecyclingSecondVC.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/4/11.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import "ZTScene.h"
#import "RecycleModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RecyclingSecondVC : ZTScene
@property (strong, nonatomic) NSMutableArray *recycle_list; //分类数组
@property (strong, nonatomic) RecycleModel * model;

@end

NS_ASSUME_NONNULL_END
