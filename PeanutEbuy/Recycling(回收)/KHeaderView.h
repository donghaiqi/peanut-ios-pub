//
//  KHeaderView.h
//  DemoForTable
//
//  Created by kangzhiqiang on 2019/8/5.
//  Copyright © 2019 kangxx. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class KHeaderView;
@class KModel;
@protocol KHeaderViewDelegate<NSObject>

- (void)headerView:(KHeaderView *)header;

@end


@interface KHeaderView : UITableViewHeaderFooterView

+ (instancetype)headerViewWithTableView:(UITableView *)tableview;

@property (nonatomic,weak) id <KHeaderViewDelegate> delegate;
@property (nonatomic,assign) NSUInteger section;
@property (nonatomic,strong) UILabel *lb;;
@property (strong, nonatomic) KModel *models;



- (void)clickSelect:(BOOL)select;
@end

NS_ASSUME_NONNULL_END
