//
//  KTableViewCell.m
//  DemoForTable
//
//  Created by kangzhiqiang on 2019/8/5.
//  Copyright © 2019 kangxx. All rights reserved.
//

#import "KTableViewCell.h"

#define  SITEMW (375-60)/4
#define  SITENH  60

@interface KTableViewCell ()

@end
@implementation KTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.imageView.image = [UIImage imageNamed:@"color_no_choose"];
    }
    return self;
}


-(void)setIsselct:(BOOL)isselct{
    if (isselct) {
        self.imageView.image = [UIImage imageNamed:@"color_choose"];
    }else{
        self.imageView.image = [UIImage imageNamed:@"color_no_choose"];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];


    // Configure the view for the selected state
}

@end
