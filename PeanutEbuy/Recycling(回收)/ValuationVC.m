//
//  ValuationVC.m
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/4/18.
//  Copyright © 2020 赵甜. All rights reserved.
//
//  估价步骤

#import "ValuationVC.h"
#import "MQGradientProgressView.h"

#import "KModel.h"
#import "KHeaderView.h"
#import "KTableViewCell.h"
#import "ClassIficationCell.h"


@interface ValuationVC ()<UITableViewDelegate,UITableViewDataSource,KHeaderViewDelegate>{
    NSUInteger selectIndex;
    KHeaderView *selectHeader;
    ClassIficationCell * deviceNameCell;
}

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *dataArr;
@property (nonatomic,strong) MQGradientProgressView *progressView;
//@property (nonatomic,strong)

@end

@implementation ValuationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"填写估价信息";
    [self add_Progress];
    [self add_DeviceNameCell];
    [self add_buttomBtn];

    
    [self addTempArray];
    [self add_tableView];
    
}

-(void) addTempArray{
    NSArray * arr1 =@[@"64G",@"128G",@"256G"];
    NSArray * arr2 =@[@"全新",@"9成新",@"8成新",@"7成新",@"6成新",];
    NSArray * arr3 =@[@"全套",@"部分",@"无",];
    
    _dataArr =[[NSMutableArray alloc]init];
    
    KModel *model = [[KModel alloc]init];
    model.visiable = YES;
    model.str = @"1.配置";
    model.array = arr1;
    [_dataArr addObject:model];
    
    KModel *model1 = [[KModel alloc]init];
    model1.visiable = NO;
    model1.str = @"2:新旧程度";
    model1.array = arr2;
    [_dataArr addObject:model1];
    
    KModel *model2 = [[KModel alloc]init];
    model2.visiable = NO;
    model2.str = @"3.附件情况";
    model2.array = arr3;
    [_dataArr addObject:model2];
    
    KModel *model3 = [[KModel alloc]init];
    model3.visiable = NO;
    model3.str = @"4.购买时间";
    [_dataArr addObject:model3];

    KModel *model4 = [[KModel alloc]init];
    model4.visiable = NO;
    model4.str = @"5.购物凭证";
    [_dataArr addObject:model4];
    
    KModel *model5 = [[KModel alloc]init];
    model5.visiable = NO;
    model5.str = @"6.拍照上传";
    model2.array = @[@""];
    [_dataArr addObject:model5];

}


-(void)add_tableView{
    UITableViewStyle style;
      if (@available(iOS 13.0, *)) {
          style =UITableViewStyleInsetGrouped;
      } else {
          style = UITableViewStyleGrouped;
      }
    
    CGRect frame = CGRectMake(0, 15+40, WIDTH, self.view.height-15-40-47-19);
    if (@available(iOS 13.0, *)) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleInsetGrouped];
    } else {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    }
    _tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    _tableView.delegate = self;
    _tableView.separatorStyle = 0;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor mainBackgroundColor];
    [self.tableView registerClass:[KTableViewCell class] forCellReuseIdentifier:@"KTableViewCell"];
    _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 10)];
    [self.view addSubview:self.tableView];
    
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(deviceNameCell.mas_bottom);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.bottom.equalTo(self.view.mas_bottom).offset(-47-19);
    }];

}

-(void)add_buttomBtn{
    UIButton * btn = [[UIButton alloc]init];
    [btn setTitle:@"马上估价" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [btn setBackgroundColor:RGB(51, 152, 218)];
    [self.view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(30);
        make.right.mas_equalTo(self.view).offset(-30);
        make.bottom.equalTo(self.view.mas_bottom).offset(-15);
        make.height.offset(47);

    }];
}

-(void)add_Progress{
    _progressView = [[MQGradientProgressView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 15)];
    _progressView.center = self.view.center;
//  progressView.colorArr = @[(id)MQRGBColor(59, 221, 255).CGColor,(id)MQRGBColor(34, 126, 239).CGColor];
    _progressView.colorArr = @[(id)[UIColor greenColor].CGColor,(id)[UIColor redColor].CGColor];
    _progressView.progress = 0.85;
    [self.view addSubview:_progressView];
    
    [_progressView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top);
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.height.offset(15);
    }];
}

-(void)add_DeviceNameCell{
    deviceNameCell=[[ClassIficationCell alloc]initWithFrame:CGRectMake(0, 15, WIDTH, 40)];
    deviceNameCell.selectionStyle = UITableViewCellSelectionStyleNone;
    [deviceNameCell addline];
    [deviceNameCell.lineLabel setHidden:NO];
    deviceNameCell.textLabel.font = [UIFont systemFontOfSize:15];
    deviceNameCell.textLabel.text = @"iPhone 7";
    [self.view addSubview:deviceNameCell];
    deviceNameCell.backgroundColor = [UIColor whiteColor];

    
    [deviceNameCell mas_remakeConstraints:^(MASConstraintMaker *make) {
           make.top.equalTo(_progressView.mas_bottom);
           make.left.equalTo(self.view.mas_left);
           make.right.equalTo(self.view.mas_right);
           make.height.offset(40);
       }];
    [deviceNameCell.lineLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(deviceNameCell.contentView.mas_top).offset(7);
        make.left.equalTo(deviceNameCell.mas_left).offset(30);
//        make.bottom.equalTo(cell.textLabel.mas_bottom);
        make.width.offset(5);
        make.height.offset(31);
    }];
    [deviceNameCell.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(deviceNameCell.mas_top);
        make.left.equalTo(deviceNameCell.lineLabel.mas_right).offset(5);
        make.bottom.equalTo(deviceNameCell.mas_bottom);
        make.right.equalTo(deviceNameCell.mas_right);
    }];
}

#pragma mark - tableview delegate / dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _dataArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    KModel *m = _dataArr[section];
    if (m.visiable) {
        return m.array.count;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 45;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    KModel *m = _dataArr[section];
    
    KHeaderView *headView = [KHeaderView headerViewWithTableView:tableView];

    headView.lb.text =[NSString stringWithFormat:@"   %@",m.str];
    headView.models = m;
    headView.delegate = self;
    headView.tag = section;
    return headView;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    KTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"KTableViewCell"];
    if (!cell) {
        cell = [[KTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"KTableViewCell"];
    }
    cell.backgroundColor =[UIColor whiteColor];
    cell.selectionStyle = 0;

    KModel *modle = _dataArr[indexPath.section];
    NSString * str = [modle.array objectAtIndex:indexPath.row];
    cell.textLabel.text = str;
    
    if (indexPath.section == 0) {
        if (indexPath.row == selectIndex) {
            cell.isselct = YES;
        }else{
            cell.isselct = NO;
        }
    }
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectIndex = indexPath.row;
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationNone];

}

- (void)headerView:(KHeaderView *)customHeaderFooterView{
    //设置组的状态
    NSIndexSet *idxSet = [NSIndexSet indexSetWithIndex:customHeaderFooterView.tag];
    [self.tableView reloadSections:idxSet withRowAnimation:UITableViewRowAnimationFade];
    
}

@end
