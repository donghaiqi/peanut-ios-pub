//
//  RecycleModel.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/5/3.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
//示例
//          "id": 2,
//        "imgUrl": "http://image.umonbe.cn/1586179391392.jpg",
//        "name": "数码"
@interface RecycleModel : NSObject
@property (nonatomic, assign)int category_id;
@property (nonatomic, assign)NSString * imgUrl;
@property (nonatomic, copy)NSString * name;
@end

@interface BrandModel : NSObject
@property (nonatomic, assign)int brand_id;
@property (nonatomic, copy)NSString * name;
@end

@interface TypeModel : NSObject
@property (nonatomic, assign)int type_id;
@property (nonatomic, copy)NSString * name;
@property (nonatomic, assign)NSString * imgUrl;

@end
NS_ASSUME_NONNULL_END
