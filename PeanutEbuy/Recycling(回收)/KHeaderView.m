//
//  KHeaderView.m
//  DemoForTable
//
//  Created by kangzhiqiang on 2019/8/5.
//  Copyright © 2019 kangxx. All rights reserved.
//

#import "KHeaderView.h"
#import "KModel.h"
@interface KHeaderView()

@end
@implementation KHeaderView

+(instancetype)headerViewWithTableView:(UITableView *)tableview
{
    static NSString *Id = @"id";
    KHeaderView *headView = [tableview dequeueReusableHeaderFooterViewWithIdentifier:Id];
    if (headView == nil) {
        headView = [[KHeaderView alloc] initWithReuseIdentifier:Id];
    }
    return headView;
}

-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}
- (void)initUI{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)];
    [self addGestureRecognizer:tap];
    _lb = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 345, 40)];
    _lb.text = @"   Title";
    _lb.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.lb];
    [_lb.layer setMasksToBounds:YES];
    [_lb.layer setCornerRadius:10];
//    [_lb mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.mas_top);
//        make.left.equalTo(self.mas_left).offset(15);
//        make.bottom.equalTo(self.mas_bottom);
//        make.right.equalTo(self.mas_right).offset(-15);
//    }];

//    UIView *linet = [UIView new];
//    linet.frame = CGRectMake(0, 0, 375, 1);
//    linet.backgroundColor = [UIColor lightGrayColor];
//    [self addSubview:linet];
    
}

- (void)tap{
    self.models.visiable = !self.models.visiable;
//    _lb.textColor = select?[UIColor redColor]:[UIColor lightGrayColor];
    [self.delegate headerView:self];
}

//- (void)clickSelect:(BOOL)select{
//}
@end
