//
//  RecyclingVC.m
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/4/11.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import "RecyclingVC.h"
#import "YSBannerView.h"
#import "AdCell.h"
#import "HomeCollectionViewCell.h"
#import "RecycleModel.h"
#import "RecyclingSecondVC.h"

@interface RecyclingVC () <YSBannerViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>{
    NSArray *_adTitles;
    UICollectionViewFlowLayout *flowLayout;
}
@property (strong, nonatomic)  YSBannerView *adBannerView;//广告
@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) NSMutableArray *recycle_list;


@end

@implementation RecyclingVC



- (void)viewDidLoad {
    [super viewDidLoad];
    _recycle_list = [[NSMutableArray alloc]init];
    [self createAdBannerView];
    [self createCollectionView];
    
    [self get_Goods_Category_List];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO; // 使右滑返回手势可用
    self.navigationController.navigationBar.hidden = NO;
    self.title = @"闲置回收";
}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = NO;
    self.hidesBottomBarWhenPushed = NO;
}


- (void)createAdBannerView{
    _adBannerView = [YSBannerView bannerViewWithFrame:CGRectMake(0, 0, self.view.width, 32)];
    _adBannerView.backgroundColor = [UIColor clearColor];
    _adBannerView.delegate = self;
    _adBannerView.scrollDirection = YSBannerViewDirectionTop;
    _adBannerView.pageControlStyle = YSPageControlNone;
    [_adBannerView disableScrollGesture];
    [self.view addSubview:_adBannerView];
    self.adBannerView.onlyDisplayText = YES;
    _adTitles = @[@"霸王防脱发，值得拥有，限时6折",
                  @"iPhoneX系列降价啦...",
                  @"恭喜用户135****2323在我平台成功换取现金￥1888元"];
    self.adBannerView.imageArray = _adTitles;
}


- (void)createCollectionView
{
    if(_collectionView == nil){

        //布局初始化
        flowLayout =[[UICollectionViewFlowLayout alloc]init];
     
        //CGRectMake(0, CGRectGetMaxY(_adBannerView.frame), self.view.width, self.view.height-CGRectGetHeight(_adBannerView.frame))
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        [self.view addSubview:_collectionView];
//        [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.left.bottom.right.equalTo(self.contentView);
//        }];
        
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor =RGB(245, 245, 245);
        
        _collectionView.showsHorizontalScrollIndicator = NO;//滚动显示
        _collectionView.showsVerticalScrollIndicator = NO;
        
        //分区头及Cell
//        [_collectionView registerNib:[UINib nibWithNibName:@"HomeCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TWCollectionViewCell"];
        [_collectionView registerClass: [UICollectionViewCell class] forCellWithReuseIdentifier:@"UICollectionViewCell"];
        
        [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_adBannerView.mas_bottom);
            make.left.equalTo(self.view.mas_left);
            make.bottom.equalTo(self.view.mas_bottom);
            make.right.equalTo(self.view.mas_right);

        }];
    }
}

#pragma mark- 获取分类列表
-(void)get_Goods_Category_List{
    WeakSelf;
    [ZTHttp recycle_Gategory_List_success:^(NSDictionary * _Nullable dic) {
        if (dic) {
            NSArray * array = [RecycleModel mj_objectArrayWithKeyValuesArray:[dic objectForKey:@"data"]];
            weakSelf.recycle_list = [NSMutableArray arrayWithArray:array];
            [weakSelf.collectionView reloadData];
        }
    } failure_Nonnull:^(NSDictionary * _Nonnull dic) {
        
    }];
}

#pragma mark - Delegate
- (Class)bannerViewRegistCustomCellClass:(YSBannerView *)bannerView {
    if (bannerView == self.adBannerView) {
        return [AdCell class];
    }
    return nil;
}

- (void)bannerView:(YSBannerView *)bannerView setupCell:(UICollectionViewCell *)customCell index:(NSInteger)index {
    if (bannerView == self.adBannerView) {
        AdCell *cell = (AdCell *)customCell;
        cell.backgroundColor = [UIColor whiteColor];
        [cell cellWithHeadHotLineCellData:_adTitles[index]];
    }
}

- (void)bannerView:(YSBannerView *)bannerView didSelectItemAtIndex:(NSInteger)index{

    NSString *titleString = @"";
    NSString *showMessage = [NSString stringWithFormat:@"点击了第%ld个", (long)index];
    
 if (bannerView == self.adBannerView){
        titleString = @"广告";
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:titleString message:showMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
    [alert show];
}

#pragma mark- collectionView delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _recycle_list.count;
}

//cell
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"UICollectionViewCell";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    if (cell==nil) {    }

   


    RecycleModel * model = [_recycle_list objectAtIndex:indexPath.row];
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.frame = cell.contentView.frame;
    [imageView sd_setImageWithURL:[NSURL URLWithString:model.imgUrl] placeholderImage:[UIImage imageNamed:@"huishou_img"]];
    [imageView.layer setMasksToBounds:YES];
    [imageView.layer setCornerRadius:6];


//    [cell.contentView setBackgroundColor:[UIColor colorWithPatternImage:imageView.image]];
    [cell.contentView addSubview:imageView];
    cell.backgroundColor = [UIColor clearColor];
    return cell;

}

#pragma mark 点击单元格
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
//    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
//    [cell setBackgroundColor:[UIColor greenColor]];
    NSString * string = [NSString stringWithFormat:@"点击%d",(int)indexPath.row];
    [self showMessage:string yOffset:1];
    
    RecyclingSecondVC * ctl =  [[RecyclingSecondVC alloc]init];
    self.hidesBottomBarWhenPushed = YES;
    ctl.recycle_list = [NSMutableArray arrayWithArray:_recycle_list];
    ctl.model = [_recycle_list objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:ctl animated:YES];
    
}

//间距
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
     return UIEdgeInsetsMake(10, 10, 10, 10);//分别为上、左、下、右
}

//大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{   float num = 0.53;
    float width = (collectionView.width-30)/2;
    return CGSizeMake(width, width*num);
}


@end
