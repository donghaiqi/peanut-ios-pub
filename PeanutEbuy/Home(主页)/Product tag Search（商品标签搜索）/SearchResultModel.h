//
//  SearchResultModel.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/5/7.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchResultModel : NSObject
@property(nonatomic,copy)NSString * mainImgUrl;
@property(nonatomic,copy)NSString * goodsName;
@property(nonatomic,copy)NSString * detailContent;
@property(nonatomic,copy)NSString * goodsNo;
@property(nonatomic,copy)NSString * goodsPrice;


@end

NS_ASSUME_NONNULL_END
