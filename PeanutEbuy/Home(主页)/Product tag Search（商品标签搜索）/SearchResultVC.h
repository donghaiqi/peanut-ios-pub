//
//  SearchResultVC.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/4/11.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import "ZTScene.h"

NS_ASSUME_NONNULL_BEGIN

@interface SearchResultVC : UIViewController
@property(nonatomic,strong)NSString * keyword;
@end

NS_ASSUME_NONNULL_END
