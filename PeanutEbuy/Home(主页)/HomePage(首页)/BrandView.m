//
//  BrandView.m
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/3/18.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import "BrandView.h"

@implementation BrandView

+(BrandView*)instanceView{
    BrandView* view= [[[NSBundle mainBundle] loadNibNamed:@"BrandView" owner:nil options:nil] objectAtIndex:0];
    return view;
}

@end
