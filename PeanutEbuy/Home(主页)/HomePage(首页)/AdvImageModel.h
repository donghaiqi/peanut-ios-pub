//
//  AdvImageModel.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/4/29.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
//示例
//        "id": 20,
//       "imgUrl": "http://image.umonbe.cn/1586170226526.jpg",
//       "type": 0,
//       "url": "https://zheshiyigeguanggao.com"

@interface AdvImageModel : NSObject

@property (nonatomic, assign)int imgId;
@property (nonatomic, assign)NSString * imgUrl;
@property (nonatomic, assign)int type;
@property (nonatomic, copy)NSString * url;

@end

NS_ASSUME_NONNULL_END
