//
//  HomeCustomView.m
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/3/17.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import "HomeCustomView.h"
#import "AdvImageModel.h"

@implementation HomeCustomView

//@property (weak, nonatomic) IBOutlet UIView *topFixldView;//头部固定
//@property (weak, nonatomic) IBOutlet UIView *goodsCategoryView;//商品类别View
//
//@property (weak, nonatomic) IBOutlet YSBannerView *middleBanner;
//@property (weak, nonatomic) IBOutlet YSBannerView *bottomBanner;
//@property (weak, nonatomic) IBOutlet UIView *showAllView;
//@property (weak, nonatomic) IBOutlet UIView *brandCategoryView;//品牌类别View


+(HomeCustomView*)instanceView{
    HomeCustomView* view= [[[NSBundle mainBundle] loadNibNamed:@"HomeCustomView" owner:nil options:nil] objectAtIndex:0];
    view.backgroundColor = [UIColor mainBackgroundColor];
    view.showAllView.backgroundColor = [UIColor mainBackgroundColor];
    view.brandCategoryView.backgroundColor =[UIColor mainBackgroundColor];
    
    //设置默认图片，网络图片，
    view.topFixldView.downloadImageBlock =
    view.middleBanner.downloadImageBlock =
    view.bottomBanner.downloadImageBlock = ^(UIImageView *imageView, NSURL *url, UIImage *placeholderImage) {
         [imageView sd_setImageWithURL:url placeholderImage:placeholderImage];
     };
    
    view.topFixldView.placeholderImage =
    view.middleBanner.placeholderImage=
    view.bottomBanner.placeholderImage= [UIImage imageNamed:@"placeholder"];
//    view.topFixldView.titleFont = [UIFont systemFontOfSize:15];
    
    view.topFixldView.scrollDirection = YSBannerViewDirectionLeft ;//滚动方向
    view.middleBanner.scrollDirection = YSBannerViewDirectionRight;
    view.bottomBanner.scrollDirection = YSBannerViewDirectionTop;
    
    
    view.topFixldView.autoScroll =
    view.middleBanner.autoScroll=
    view.bottomBanner.autoScroll=YES;
    
    view.topFixldView.showPageControl =
       view.middleBanner.showPageControl=
       view.bottomBanner.showPageControl=YES;
    
    view.goodsCategoryView.backgroundColor = [UIColor whiteColor];
    [view.goodsCategoryView.layer setMasksToBounds:YES];
    [view.goodsCategoryView.layer setCornerRadius:6.0f];
    
    return view;
}

-(void)UpdataUi{
  
    [self layoutIfNeeded];
    [self httpGetAdvImage];
    [self initGoodsUnitView];
    [self initBrandUnitView];

    NSArray *imageArray = @[@"http://img.zcool.cn/community/01430a572eaaf76ac7255f9ca95d2b.jpg",
                             @"http://img.zcool.cn/community/0137e656cc5df16ac7252ce6828afb.jpg",
                         @"http://img.zcool.cn/community/01e5445654513e32f87512f6f748f0.png@900w_1l_2o_100sh.jpg",
                             ];
    
    NSArray *imageArray1 = @[
        @"http://img.zcool.cn/community/01430a572eaaf76ac7255f9ca95d2b.jpg",
                             @"http://img.zcool.cn/community/0137e656cc5df16ac7252ce6828afb.jpg",
        @"http://img.zcool.cn/community/01e5445654513e32f87512f6f748f0.png@900w_1l_2o_100sh.jpg",

                             ];
    //顶部
    _topFixldView.autoScrollTimeInterval = 2;
//    _topFixldView.titleArray = @[@"我是第一个标题"];
    _topFixldView.delegate = self;
    _topFixldView.imageArray = imageArray;
    [_topFixldView disableScrollGesture];
    
//    中部
    _middleBanner.autoScrollTimeInterval = 2;
    _middleBanner.delegate = self;
    _middleBanner.imageArray = imageArray1;
//    [_topFixldView disableScrollGesture];
//    底部
    _bottomBanner.autoScrollTimeInterval = 2;
    _bottomBanner.delegate = self;
    _bottomBanner.imageArray = imageArray;
    _bottomBanner.showPageControl = NO;

//    [_topFixldView disableScrollGesture];
}

//
-(void)httpGetAdvImage{
    WeakSelf([HomeCustomView class]);
    [ZTHttp getAdvImage:1 success:^(NSDictionary * _Nullable dic) {
        if (dic) {
            weakSelf.resultArray = [AdvImageModel mj_objectArrayWithKeyValuesArray:[dic objectForKey:@"data"]];
            NSMutableArray * advImageArray =[[NSMutableArray alloc]init];
            for (int a=0; a<weakSelf.resultArray.count; a++) {
                AdvImageModel * model = [weakSelf.resultArray objectAtIndex:a];
                [advImageArray addObject:model.imgUrl];
            }
            weakSelf.topFixldView.imageArray = advImageArray;
        }
    } failure_Nonnull:^(NSDictionary * _Nonnull dic) {
        ZTLog(@"轮播图请求错误");
    }];
    
}

-(void)initGoodsUnitView{
    //测试数据：
    NSArray * imageArry = @[@"icon1",@"icon2",@"icon3",@"icon4",@"icon5"];
    NSArray * titleArry =@[@"商品分类",@"服务分类",@"热销产品",@"热搜产品",@"邀请有礼"];
    
    //页面
    float interval = 10.0f;
    float width = (_goodsCategoryView.width - interval*6)/5;
    float height = (_goodsCategoryView.height - interval*2);

    for (int a = 0; a<imageArry.count; a++) {
        int column = a%5;//列
        int line = a/5;//行
        UIView * unitView = [[UIView alloc]init];
        unitView.frame = CGRectMake(interval *(column+1)+column*width, (line+1)*interval+line*height, width, height);
        unitView.backgroundColor = [UIColor whiteColor];
        
        UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickUnit:)];
        [unitView addGestureRecognizer:tapGes];
        [tapGes view].tag = a;
        [_goodsCategoryView addSubview:unitView];
        
        UIImageView * imageView =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, unitView.width, unitView.width)];
        imageView.image = [UIImage imageNamed:[imageArry objectAtIndex:a]];
        imageView.backgroundColor = [UIColor clearColor];
        [unitView addSubview:imageView];
        
        UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(0, imageView.height, unitView.width, unitView.height-imageView.height)];
        label.font = [UIFont systemFontOfSize:11 weight:0.7];
        label.textAlignment = NSTextAlignmentCenter;
        label.text =[titleArry objectAtIndex:a];
        [unitView addSubview:label];
    }
}


-(void)initBrandUnitView{
    
    float interval = 4.5f;
    float width = (_brandCategoryView.width - interval*2)/3;
    float height = _brandCategoryView.height;
    for (int a = 0 ; a<3; a++) {
        //品牌
        BrandView * view  = [BrandView instanceView];
        view.frame = CGRectMake(a*interval+a*width, 0, width, height);
        [_brandCategoryView addSubview:view];
    }

}


-(void)clickUnit:(id)sender{
    if ([sender isKindOfClass:[UITapGestureRecognizer class]]==YES) {
         UITapGestureRecognizer *singleTap = (UITapGestureRecognizer *)sender;
        int clickTag = (int)[singleTap view].tag;
        if (_jumpToClassification) {
            self.jumpToClassification(clickTag);
        }
    }
}

- (IBAction)clickShowAllBtn:(UIButton *)sender {
//    跳转
    if (_jumpToClassification) {
//        self.jumpToClassification();
    }
}


#pragma mark - Delegate
- (Class)bannerViewRegistCustomCellClass:(YSBannerView *)bannerView {

    return nil;
}

- (void)bannerView:(YSBannerView *)bannerView setupCell:(UICollectionViewCell *)customCell index:(NSInteger)index {

}

- (void)bannerView:(YSBannerView *)bannerView didSelectItemAtIndex:(NSInteger)index{

    AdvImageModel * model = [_resultArray objectAtIndex:index];
    [ZTHttp clickAdvImage:model.imgId success:^(NSDictionary * _Nullable dic) {
        if (dic) {
            NSString *titleString = @"";
            NSString *showMessage = [NSString stringWithFormat:@"点击了第%ld个", (long)index];
            if (bannerView == self.topFixldView) {
                titleString = @"BannerView";
            }
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:titleString message:showMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
            [alert show];
        }
    } failure_Nonnull:^(NSDictionary * _Nonnull dic) {
        ZTLog(@"点击广告图片网络请求错误");
    }];

}





@end
