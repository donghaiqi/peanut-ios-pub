//
//  CollectionViewCell.m
//  CollectionView
//
//  Created by tang on 16/3/4.
//  Copyright © 2016年 tang. All rights reserved.
//

#import "HomeCollectionViewCell.h"

@implementation HomeCollectionViewCell

//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"HomeCollectionViewCell" owner:nil options:nil];
//
//    UIView *plainView = [nibContents lastObject];
//    CGSize padding = (CGSize){ 0, 0 };
//    plainView.frame = (CGRect){padding.width, padding.height, plainView.frame.size};
//    // Add to the view hierarchy (thus retain).
//    [self.contentView addSubview:plainView];
//    return self;
//}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self initUI];
}

- (void)initUI {
    [self.similarBtn.layer setMasksToBounds:YES];
    [self.similarBtn.layer setCornerRadius:_similarBtn.height/2];
    self.similarBtn.layer.borderColor =RGBA(186, 186, 186, 1).CGColor;
    self.similarBtn.layer.borderWidth = 1;
    
    self.layer.cornerRadius = 6;
    self.backgroundColor = [UIColor whiteColor];
    [self.layer setMasksToBounds:YES];

}

@end
