//
//  CollectionViewCell.h
//  CollectionView
//
//  Created by tang on 16/3/4.
//  Copyright © 2016年 tang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ImageView;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton *similarBtn;

//memberspriceLabel
//@property (nonatomic, retain)UIImageView *imageView;
//@property (nonatomic, retain)UILabel *label;

@end
