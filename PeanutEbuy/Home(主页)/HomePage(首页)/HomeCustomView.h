//
//  HomeCustomView.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/3/17.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YSBannerView.h"
#import "BrandView.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^JumpToClassification)(int selectIndex);

@interface HomeCustomView : UIView<YSBannerViewDelegate>

@property (weak, nonatomic) IBOutlet YSBannerView *topFixldView;//头部固定
@property (weak, nonatomic) IBOutlet UIView *goodsCategoryView;//商品类别View

@property (weak, nonatomic) IBOutlet YSBannerView *middleBanner;
@property (weak, nonatomic) IBOutlet YSBannerView *bottomBanner;
@property (weak, nonatomic) IBOutlet UIView *showAllView;
@property (weak, nonatomic) IBOutlet UIView *brandCategoryView;//品牌类别View

@property(nonatomic ,strong)NSMutableArray * resultArray;

@property (copy, nonatomic)  JumpToClassification jumpToClassification;


+(HomeCustomView*)instanceView;

- (IBAction)clickShowAllBtn:(UIButton *)sender;

-(void)UpdataUi;
-(void)initGoodsUnitView;
-(void)initBrandUnitView;


@end

NS_ASSUME_NONNULL_END
