//
//  BrandView.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/3/18.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BrandView : UIView
+(BrandView*)instanceView;
@property (weak, nonatomic) IBOutlet UIImageView *iamgeView;
@property (weak, nonatomic) IBOutlet UIButton *btn;
@property (weak, nonatomic) IBOutlet UILabel *label;

@end

NS_ASSUME_NONNULL_END
