//
//  HomeVC.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/3/16.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZTnavigationBar.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeVC : ZTScene
@property(nonatomic, strong)ZTnavigationBar * ztNavigationBar;

@end

NS_ASSUME_NONNULL_END
