//
//  CityModel.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/5/8.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LocationCityModel : NSObject

@property(nonatomic,assign)int level;
@property(nonatomic,assign)int city_id;
@property(nonatomic,copy)NSString * name;
@property(nonatomic,copy)NSString * zipCode;
@property(nonatomic,copy)NSString * letter;
@property(nonatomic,assign)int parentId;

@end

NS_ASSUME_NONNULL_END
