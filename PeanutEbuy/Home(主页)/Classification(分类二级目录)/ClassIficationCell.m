//
//  ClassIficationCell.m
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/3/29.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import "ClassIficationCell.h"

@implementation ClassIficationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)addline{
    self.backgroundColor =[UIColor mainBackgroundColor];
    _lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 7, 5, 31)];
    _lineLabel.backgroundColor = RGB(51, 152, 218);
    _lineLabel.layer.masksToBounds =YES;
    _lineLabel.layer.cornerRadius =2.5;
    [self addSubview:_lineLabel];
    [_lineLabel setHidden:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
         self.textLabel.textColor = RGB(51, 152, 218);
        self.backgroundColor =[UIColor whiteColor];
        [_lineLabel setHidden:NO];
     }
     else {
         self.textLabel.textColor = [UIColor blackColor];
         self.backgroundColor =[UIColor mainBackgroundColor];
         [_lineLabel setHidden:YES];
     }
    // Configure the view for the selected state
}

@end
