//
//  ClassCollectionCell.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/3/30.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ClassCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

NS_ASSUME_NONNULL_END
