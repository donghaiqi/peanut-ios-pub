//
//  ClassIficationCell.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/3/29.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ClassIficationCell : UITableViewCell
@property(strong,nonatomic)UILabel *lineLabel;

-(void)addline;
@end

NS_ASSUME_NONNULL_END
