//
//  SceneDelegate.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/3/3.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

