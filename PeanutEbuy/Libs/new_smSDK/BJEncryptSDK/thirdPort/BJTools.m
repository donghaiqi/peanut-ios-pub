//
//  BJTools.m
//  kaduoduo
//
//  Created by 黑核数据 on 2018/6/29.
//  Copyright © 2018年 wangjianchao. All rights reserved.
//

#import "BJTools.h"
#import "GTMBase64.h"
@implementation BJTools
+(NSString *)nvlStr:(NSString *)str
{
    if (str==nil||[str isKindOfClass:[NSNull class]]||(![str isKindOfClass:[NSString class]])) {
        return @"";
    }
    return str;
    
}
+(NSString *)nslStr:(NSString *)str replaceStr:(NSString *)replaceStr
{
    if (str==nil||[str isKindOfClass:[NSNull class]]||(![str isKindOfClass:[NSString class]])) {
        return replaceStr;
    }
    return str;
}
+(void)setTempValue:(id)value forKey:(NSString *)key
{
    value = [BJTools nvlStr:value];
    [[NSUserDefaults standardUserDefaults] setValue:value forKey:key];
}
+(NSString *)getTempValueForKey:(NSString *)key
{
    NSString * value = [[NSUserDefaults standardUserDefaults] valueForKey:key];
    value = [BJTools nvlStr:value];
    return value;
}
+(NSString *)base64EnCode:(NSString *)str
{
    NSData * data = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSString * enCodeStr = [GTMBase64 stringByEncodingData:data];
    enCodeStr = [self addCode:enCodeStr];
    return enCodeStr;
}
+(NSString *)base64DeCode:(NSString *)str
{
    str = [self removeCode:str];
    NSData * deCodeData = [GTMBase64 decodeString:str];
    NSString * relStr = [[NSString alloc]initWithData:deCodeData encoding:NSUTF8StringEncoding];
    return relStr;
}
+(NSString *)addCode:(NSString *)str
{
    NSString * pstr = @"asdfghjklzxcvbnmqwertyuiopQWERTYUIOPASDFGHJKLZXCVBNM";
    NSMutableString * addStr = [NSMutableString string];
    for (int i = 0; i< 40; i++) {
        NSInteger n = arc4random()%pstr.length;
        NSString * s = [pstr substringWithRange:NSMakeRange(n, 1)];
        [addStr appendString:s];
    }
    return [NSString stringWithFormat:@"%@%@%@",addStr,str,addStr];
}
+(NSString *)removeCode:(NSString *)str
{
    
    if (str.length >80) {
        NSInteger length = str.length - 80;
        return [str substringWithRange:NSMakeRange(40, length)];
    }
    return @"";
}
@end
