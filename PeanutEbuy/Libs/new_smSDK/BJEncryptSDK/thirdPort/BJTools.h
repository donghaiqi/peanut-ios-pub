//
//  BJTools.h
//  kaduoduo
//
//  Created by 黑核数据 on 2018/6/29.
//  Copyright © 2018年 wangjianchao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BJTools : NSObject
//字符串判空处理
+(NSString *)nvlStr:(NSString *)str;//如果为nil 直接返回空字符串
+(NSString *)nslStr:(NSString *)str replaceStr:(NSString *)replaceStr;//如果为nil替换为replaceStr

//设置临时值
+(void)setTempValue:(id)value forKey:(NSString *)key;
//获取临时值
+(NSString *)getTempValueForKey:(NSString *)key;


//加密
+(NSString *)base64EnCode:(NSString *)str;
//解密
+(NSString *)base64DeCode:(NSString *)str;
@end
