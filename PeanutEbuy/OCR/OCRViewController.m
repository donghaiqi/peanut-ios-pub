//
//  ViewController.m
//  heiheSDKdemo
//
//  Created by pospt on 2018/11/26.
//  Copyright © 2018年 王健超. All rights reserved.
//

#import "OCRViewController.h"
//#import "OCRSDK.h"
#import "bankIDFrontVCViewController.h"
#import "IDcardFrontVCViewController.h"
@interface OCRViewController ()

@end

@implementation OCRViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"黑核OCR 测试";
    
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    NSArray *titiArr=@[@"身份证正面扫描",@"银行卡正面扫描",@"营业执照扫描"];
    for(int i=0;i<titiArr.count;i++)
    {
        UIButton *selectButton=[UIButton buttonWithType:UIButtonTypeCustom];
        selectButton.frame=CGRectMake((self.view.frame.size.width-150)/2, 300+100*i, 150, 60);
        [selectButton setTitle:titiArr[i] forState:UIControlStateNormal];
        selectButton.backgroundColor=[UIColor grayColor];
        [selectButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        selectButton.tag=100+i;
        [selectButton addTarget:self action:@selector(selectBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:selectButton];
    }
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)selectBtn:(UIButton *)select
{
   
    /*
     type: 0身份证正面  1身份证反面   2银行卡正面   3营业执照
     superVC: 调用OCR识别的发起页面
     backImage: 识别成功后图片
     infoDic: 识别成功后返回的识别信息
     */
     /*
    switch (select.tag) {
        case 100:
            {
                [OCRSDK scanCardInfoWithType:(int)(0) formVC:self andFinishedBlock:^(UIImage *backImage, NSDictionary *infoDic) {
                    //OCR识别返回信息  backImage 返回图片  infoDic: 返回的数据信息
                    NSLog(@"%@~~~~~~~%@",infoDic,backImage);
                    [self drowIDcardFrontImage:backImage andInfoDic:infoDic];
                    // [self drowbankIDFrontImage:backImage andInfoDic:infoDic];
                }];
                break;
            }
           case 101:
        {
            [OCRSDK scanCardInfoWithType:(int)(2) formVC:self andFinishedBlock:^(UIImage *backImage, NSDictionary *infoDic) {
                //OCR识别返回信息  backImage 返回图片  infoDic: 返回的数据信息
                
                NSLog(@"%@~~~~~~~%@",infoDic,backImage);
                
                [self drowbankIDFrontImage:backImage andInfoDic:infoDic];
                
            }];
            
            break;
        }
            case 102:
                  {
                      [OCRSDK scanCardInfoWithType:(int)(3) formVC:self andFinishedBlock:^(UIImage *backImage, NSDictionary *infoDic) {
                          //OCR识别返回信息  backImage 返回图片  infoDic: 返回的数据信息
                          
                          NSLog(@"%@~~~~~~~%@",infoDic,backImage);
                          
                          [self drowbankIDFrontImage:backImage andInfoDic:infoDic];
                          
                      }];
                      
                      break;
                  }
        default:
            break;
    }
    */
}
#pragma mark 获取身份证正面相关信息
-(void)drowIDcardFrontImage:(UIImage *)image andInfoDic:(NSDictionary *)dic
{
    __block NSDictionary * weakDic = dic[@"data"];
    dispatch_async(dispatch_get_main_queue(), ^{
        IDcardFrontVCViewController *IDcardvc=[[IDcardFrontVCViewController alloc] init];
        IDcardvc.getImage=image;
        IDcardvc.infoDictionary=weakDic;
        [self.navigationController pushViewController:IDcardvc animated:YES];
    });
}
#pragma mark 获取银行卡正面相关信息
-(void)drowbankIDFrontImage:(UIImage *)image andInfoDic:(NSDictionary *)dic
{
    __block NSDictionary * weakDic = dic[@"data"];
    dispatch_async(dispatch_get_main_queue(), ^{
        bankIDFrontVCViewController *bankvc=[[bankIDFrontVCViewController alloc] init];
        bankvc.getImage=image;
        bankvc.infoDictionary=weakDic;
        [self.navigationController pushViewController:bankvc animated:YES];
    });
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
