//
//  IDcardFrontVCViewController.m
//  heiheSDKdemo
//
//  Created by pospt on 2018/11/26.
//  Copyright © 2018年 王健超. All rights reserved.
//

#import "IDcardFrontVCViewController.h"

@interface IDcardFrontVCViewController ()

@end

@implementation IDcardFrontVCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title=@"身份证信息";
    self.view.backgroundColor=[UIColor whiteColor];
    NSLog(@"输出打印卡片信息%@",self.infoDictionary);
    
    [self getInfo];
    // Do any additional setup after loading the view.
}
-(void)getInfo
{
    
    /*
     address   地址
     birth     出生年月
     id        身份证号码
     name      姓名
     nation    民族
     sex       性别
     */
    NSArray *infoKeyArr=@[@"address",@"birth",@"id",@"name",@"nation",@"sex"];
    NSArray *infoArr=@[@"地址",@"出生年月",@"身份证号码",@"姓名",@"民族",@"性别"];
    UIImageView *IDcardImage=[[UIImageView alloc] initWithFrame:CGRectMake((self.view.frame.size.width-280)/2, 100, 280, 185)];
    IDcardImage.image=self.getImage;
    [self.view addSubview:IDcardImage];
    
    for(int i=0;i<infoArr.count;i++)
    {
        UILabel *leftLab=[[UILabel alloc] init];
        leftLab.text=infoArr[i];
        leftLab.font=[UIFont systemFontOfSize:11];
        CGSize leftSize=[leftLab.text sizeWithAttributes:@{NSFontAttributeName:leftLab.font}];
        leftLab.frame=CGRectMake(20, 400+50*i, leftSize.width, 40);
        [self.view addSubview:leftLab];
        
        UILabel *rightLab=[[UILabel alloc] init];
        rightLab.text=[self.infoDictionary objectForKey:infoKeyArr[i]];
        rightLab.font=[UIFont systemFontOfSize:11];
        rightLab.frame=CGRectMake(leftLab.frame.origin.x+leftSize.width+10, 400+50*i,(self.view.frame.size.width-leftLab.frame.origin.x-leftSize.width-20), 40);
        rightLab.textAlignment=NSTextAlignmentLeft;
        [self.view addSubview:rightLab];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
