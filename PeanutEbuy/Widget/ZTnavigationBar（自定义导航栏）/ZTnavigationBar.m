//
//  ZTnavigationBar.m
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/3/16.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import "ZTnavigationBar.h"

@implementation ZTnavigationBar


+(ZTnavigationBar*)instanceView{
    ZTnavigationBar* view= [[[NSBundle mainBundle] loadNibNamed:@"ZTnavigationBar" owner:nil options:nil] objectAtIndex:0];
    
    return view;
}


-(void)update{

    [self mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self.superview);
        make.height.mas_equalTo(kNavHeight);
    }];
    
    int spaceHeight = 44;
    
    self.frame = CGRectMake(0, 0, currentDeviceWidth, kNavHeight);
    self.citybtn.frame = CGRectMake(0, viewBottom(self)-spaceHeight, 71, spaceHeight);
    self.carBtn.frame = CGRectMake(currentDeviceWidth - spaceHeight, viewBottom(self)-spaceHeight, 37, spaceHeight);
    self.messageBtn.frame = CGRectMake(currentDeviceWidth-37*2, viewBottom(self)-spaceHeight, 37, spaceHeight);
    self.ztsearchBar.frame = CGRectMake(self.citybtn.width, viewBottom(self)-spaceHeight, currentDeviceWidth-37*2-self.citybtn.width, spaceHeight);
//    self.ztsearchBar.delegate = self;

       
        //修改背景
    UIImage * searchBarBg = [self GetImageWithColor:[UIColor clearColor] andHeight:spaceHeight];
    [self.ztsearchBar setBackgroundImage:searchBarBg];
//        self.ztsearchBar.barTintColor = [UIColor whiteColor];

        //更改输入框字号
     UITextField  *seachTextFild = (UITextField*)[self subViewOfClassName:@"UISearchBarTextField" view:self.ztsearchBar];
     seachTextFild.font = [UIFont systemFontOfSize:16];
    seachTextFild.backgroundColor = [UIColor whiteColor];
    seachTextFild.layer.borderColor = [UIColor blackColor].CGColor;
    seachTextFild.layer.borderWidth = 0.5;
    seachTextFild.layer.cornerRadius = 17.5;
    seachTextFild.layer.masksToBounds = YES;
    
//    //修改输入栏高度，测试
//         CGFloat height = self.ztsearchBar.height;
//         CGFloat top = (height - 20.0) / 2.0;
//         CGFloat bottom = top;
//        _ztsearchBar.contentInset = UIEdgeInsetsMake(top, 0, bottom, 0);

}

- (UIView*)subViewOfClassName:(NSString*)className view:(UIView *)view{
    for (UIView* subView in view.subviews) {
        NSLog(@"======%@",subView.class);
        if ([NSStringFromClass(subView.class) isEqualToString:className]) {
            return subView;
        }
        UIView* resultFound = [self subViewOfClassName:className view:subView];
        if (resultFound) {
            return resultFound;
        }
    }
    return nil;
}


/**
 *  生成图片
 *
 *  @param color  图片颜色
 *  @param height 图片高度
 *
 *  @return 生成的图片
 */
- (UIImage*) GetImageWithColor:(UIColor*)color andHeight:(CGFloat)height
{
    CGRect r= CGRectMake(0.0f, 0.0f, 1.0f, height);
    UIGraphicsBeginImageContext(r.size);
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, r);

    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return img;
}

//重写layoutSubviews 修改textfield高度

#pragma mark 点击事件
- (IBAction)clickCitybtn:(id)sender {
//    typedef void (^IndexChangeBlock)(NSInteger index);
    ZTLog(@"点击城市按钮")
    self.cityBtnBlcok();

}

- (IBAction)clickMessageBtn:(id)sender {
    ZTLog(@"点击消息按钮")
    self.messageBtnBlcok();

}

- (IBAction)clickCarBtn:(id)sender {
    ZTLog(@"点击购物车按钮")
    self.carBtnBlcok();
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    self.beginEditingBlcok();
    return NO;
}

@end
