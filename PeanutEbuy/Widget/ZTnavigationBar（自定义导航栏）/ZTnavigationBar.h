//
//  ZTnavigationBar.h
//  PeanutEbuy
//
//  Created by 赵甜 on 2020/3/16.
//  Copyright © 2020 赵甜. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


typedef void(^CityBtnBlcok)(void);
typedef void(^MessageBtnBlcok)(void);
typedef void(^CarBtnBlcok)(void);
typedef void(^BeginEditingBlcok)(void);



@interface ZTnavigationBar : UIView//<UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UIButton *citybtn;
@property (weak, nonatomic) IBOutlet UISearchBar *ztsearchBar;


@property (weak, nonatomic) IBOutlet UIButton *messageBtn;
@property (weak, nonatomic) IBOutlet UIButton *carBtn;

@property (copy, nonatomic) CityBtnBlcok cityBtnBlcok;
@property (copy, nonatomic) MessageBtnBlcok messageBtnBlcok;
@property (copy, nonatomic) CarBtnBlcok carBtnBlcok;
@property (copy, nonatomic) BeginEditingBlcok beginEditingBlcok;



+(ZTnavigationBar*)instanceView;
- (IBAction)clickCitybtn:(id)sender;
- (IBAction)clickMessageBtn:(id)sender;
- (IBAction)clickCarBtn:(id)sender;

-(void)update;
- (UIView*)subViewOfClassName:(NSString*)className view:(UIView *)view;
- (UIImage*) GetImageWithColor:(UIColor*)color andHeight:(CGFloat)height;



@end

NS_ASSUME_NONNULL_END
